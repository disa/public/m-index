/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.operations.RankingSingleQueryOperation;
import mindex.MetricIndex;

/**
* This query-PPP distance calculator uses the query-pivot distances as a weighted sum of the 
* query-pivot distances.
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */ 
public class WeightedSumDistCalculator extends QueryPivotDistDiffCalculator {

    /** The default value of the weighting coefficient */
    public static final float DEFAULT_COEF = 0.75f;
    /** Name of the parameter that can set the coefficient */
    public static final String WEIGHTED_SUM_COEF_PARAM = "WEIGHTED_SUM_COEF_PARAM";
    
    /** Precomputed coefficients for each level (c^0, c^1, c^2,..., c^{l-1}) */
    protected float [] COEFS;
    /** Sums of the coefficients from the level 0 to the actual level 'i' */
    protected float [] COEF_SUMS;
    
    /**
     * Initializes the calculator from an operation (query object and coefficient parameter).
     * @param operation query operation
     * @param mIndex current metric index
     * @throws AlgorithmMethodException if initialization of the query object did not work (query-pivot distances)
     */
    public WeightedSumDistCalculator(RankingSingleQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(operation.getQueryObject(), mIndex);
        initCoeficients(operation.getParameter(WEIGHTED_SUM_COEF_PARAM, Float.class, DEFAULT_COEF));
    }
    
    protected WeightedSumDistCalculator(LocalAbstractObject queryObject, MetricIndex mIndex, float coeficient) throws AlgorithmMethodException {
        super(queryObject, mIndex);
        initCoeficients(coeficient);
    }
    
    protected WeightedSumDistCalculator(short maxLevel, float [] queryPivotDistances, short [] queryPPP) {
        super(maxLevel, queryPivotDistances, queryPPP);
        initCoeficients(DEFAULT_COEF);
    }
    
    private void initCoeficients(float coeficient) {
        COEFS = new float[maxLevel];
        COEF_SUMS = new float[maxLevel];
        COEFS[0] = 1f;
        COEF_SUMS[0] = COEFS[0];
        for (int i = 1; i < maxLevel; i++) {
            COEFS[i] = COEFS[i-1] * coeficient;
            COEF_SUMS[i] = COEF_SUMS[i-1] + COEFS[i];
        }
    }

    // **************   Distance calculations    ************************** //
    
    @Override
    public float getQueryDistance(final short[] ppp) {
        //return getDistance(ppp, 1, getMaxLevel(ppp.length));
        int toLevel = getMaxLevel(ppp.length);
        float sum = 0f;
        for (int i = 0; i < toLevel; i++) {
            sum += (queryPivotDistances[ppp[i]] * COEFS[i]);
        }
        return sum;
    }


    /**
     * Returns the estimation of the query-object distances (weighted average not weighted sum)
     * @param ppp PPP to estimate the distance for
     * @return estimated distance
     */
    public final float getCellDistanceEstimation(short[] ppp) {        
        float sum = 0f;
        int currentMaxLevel = getMaxLevel(ppp.length);
        for (int i = 0; i < currentMaxLevel; i++) {
            sum += (queryPivotDistances[ppp[i]] * COEFS[i]);
        }
        
        return (ppp.length == 0) ? sum : sum / COEF_SUMS[currentMaxLevel - 1];
    }

}
