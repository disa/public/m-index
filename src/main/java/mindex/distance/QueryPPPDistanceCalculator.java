/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.distance;

import mindex.navigation.NextLevelPivotChecker;

/**
 * This interface calculates distance between a query (typically represented by full PP or query-pivot
 *  distances) and given pivot permutation prefix.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface QueryPPPDistanceCalculator {

    /**
     * Calculate distance between a query (typically represented by full PP or query-pivot
     *  distances) and given pivot permutation prefix.
     * @param ppp pivot permutation prefix of some length
     * @return distance between query and given PPP
     */
    public float getQueryDistance(short [] ppp);

    /**
     * Calculate distance between a query (typically represented by full PP or query-pivot
     *  distances) and PPP of given cell of the PPP tree.
     * @param cell internal or leaf cell of the PPP tree.
     * @return distance between query and given node PPP
     */
    public float getQueryDistance(short [] ppp, final NextLevelPivotChecker cell);
    
    /**
     * Returns the array with distances between the query and the pivots
     * @return the array with distances between the query and pivots
     */
    public float[] getQueryPivotDistances();

    /**
     * Returns the pivot permutation prefix of the query (length specified by actual the M-Index configuration)
     * @return pivot permutation prefix of the query 
     */
    public short[] getQueryPPP();

    /**
     * Returns number of query-PPP distance calculation realized by this calculator so far.
     * @return number of query-PPP distance calculation realized by this calculator so far (in all threads)
     */
    public int getDistCalculationStat();

    public int getMaxLevel();
    
    /**
     * For testing reasons, this method returns the minimum of the a priori specified max level and the
     *   current max level passed in the parameter.
     * @param currentMax
     * @return 
     */
    public int getMaxLevel(int currentMax);
    
    public void setMaxLevel(int maxLevel);
    
}
