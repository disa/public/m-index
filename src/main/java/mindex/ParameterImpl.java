/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

/**
 * Straightforward implementation of the Parameter interface.
 *  It stores <ul>
 *  <li>property key</li>
 *  <li>value type</li>
 *  <li>default value</li>
 *  <li>comment text</li>
 *  <li>internally used class of parameters (for M-Index configuration process)</li>
 *  <li>flag saying if the a this param should be used via a variable to be passed from the messif-start script</li>
 * </ul>
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
 public class ParameterImpl implements Parameter {
    
     
     // ***********************               Fields            ********************** //
     
     private final ParamClass paramClass;
     private final String key;
     private final Type type;
     private final String defaultValue;
     private final String comment;
     private final boolean useMessifStartVariable;

     /**
      * Class of parameter that is used in the semi-automatic M-Index configuration process.
      */
     public enum ParamClass {
         MANDATORY,
         OTHER,
         OVERFILLED,
         DISK_STORAGE,
         INTERNALMINDEX
     }
     
     @Override
     public String getKey() {
         return key;
     }

     @Override
     public Type getType() {
         return type;
     }

     @Override
     public String getDefaultValue() {
         return defaultValue;
     }

     @Override
     public String getComment() {
         return comment;
     }

     public boolean useMessifStartVariable() {
         return useMessifStartVariable;
     }

    public ParamClass getParamClass() {
        return paramClass;
    }

     /**
      * Internal private constructor setting all fields.
      *
      * @param paramClass type of the parameter {@link #paramClass}
      * @param key property key of this parameter
      * @param type type of the parameter
      * @param defaultValue default string
      * @param comment comment string (can be empty)
      * @param useMessifStartVariable if true, value of the parameter is not set directly in the .cf file but is passed
      * from the .defaults file as a variable
      */
     public ParameterImpl(ParamClass paramClass, String key, Type type, String defaultValue, String comment, boolean useMessifStartVariable) {
         this.paramClass = paramClass;
         this.key = key;
         this.type = type;
         this.defaultValue = defaultValue;
         this.comment = comment;
         this.useMessifStartVariable = useMessifStartVariable;
     }
}
