/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import java.util.List;
import messif.buckets.Bucket;
import messif.objects.LocalAbstractObject;

/**
 * An auxiliary class encapsulating if agile multibucket partitioning should be 
 * used during a split + number of objects that was really inserted into the bucket by the
 * operation that caused the split/
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class SplitConfiguration {
    
    /** Number of objects that was really inserted by the last insert operation */
    private final int numberOfInsertedObjects;
    
    /** Flag determining if agile multi bucket partitioning should be used */
    private final boolean agileMultibucketPartitioning;
    
    /** List of objects to be reinserted after the split */
    private final List<? extends LocalAbstractObject> toReinsert;
    
    /** Store information about the bucket that was to be split */
    private final Bucket bucketToBeSplit;
    
    public SplitConfiguration(boolean agileMultibucketPartitioning, int numberOfInsertedObjects, List<? extends LocalAbstractObject> toReinsert, Bucket bucketToBeSplit) {
        this.agileMultibucketPartitioning = agileMultibucketPartitioning;
        this.numberOfInsertedObjects = numberOfInsertedObjects;
        this.toReinsert = toReinsert;
        this.bucketToBeSplit = bucketToBeSplit;
    }
    
    public boolean isAgileMultibucketPartitioning() {
        return agileMultibucketPartitioning;
    }
    
    public int getNumberOfInsertedObjects() {
    	return numberOfInsertedObjects;
    }

    public List<? extends LocalAbstractObject> getToReinsert() {
        return toReinsert;
    }        

    public Bucket getBucketToBeSplit() {
        return bucketToBeSplit;
    }
}
