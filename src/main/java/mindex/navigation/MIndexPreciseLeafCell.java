/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import java.util.List;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.Bucket;
import messif.objects.LocalAbstractObject;
import mindex.MIndexKey;
import mindex.MIndexKeyInterval;
import mindex.MIndexKeySimple;
import mindex.MIndexPP;
import mindex.MIndexPPSimple;
import mindex.MetricIndex;
import mindex.MetricIndexes;

/**
 * Abstract class encapsulates a leaf node of a cluster tree for a dynamic M-Index when the precise
 *  search strategies should be efficiently applied (the key interval is stored).
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexPreciseLeafCell extends MIndexLeafCell {

    /** Class id for serialization. */
    private static final long serialVersionUID = 41501L;
    
    /** My M-Index key with the PP */
    private final MIndexPPSimple myKeyPP;
    
    /** The interval covered by the data stored locally in this node's storage. */
    private MIndexKeyInterval coveredInterval;

    /**
     * Constructor given all parameters mandatory for the parent class. The storage and the covered interval are set to null.
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     * @param pivotCombination the pivot combination corresponding to this cluster, e.g. [3,2,4] for cluster C_{3,2,4}
     */
    public MIndexPreciseLeafCell(MetricIndex mIndex, MIndexPreciseInternalCell parentNode, short[] pivotCombination) {
        super(mIndex, parentNode, pivotCombination);
        myKeyPP = new MIndexPPSimple(pivotCombination, mIndex.getMaxLevel(), mIndex.getNumberOfPivots());
        MIndexKey myMIndexKey = new MIndexKeySimple(myKeyPP.getClusterNumber((short) pivotCombination.length), 0f);
        coveredInterval = new MIndexKeyInterval(myMIndexKey, myMIndexKey);
    }
        
    // ***********************      Getters and Setters   ********************************* //
        
    public MIndexKeyInterval getCoveredInterval() {
        return coveredInterval;
    }

    public MIndexPP getPivotPermutation(){
        return myKeyPP;
    }
    
    @Override
    public short getLevel() {
        return myKeyPP.getPPLevel();
    }

    // ***************************      Data manipulation extensions    ********************* //
    
    /**
     * Tries to insert all the objects into the bucket managed by this LeafCell.
     * If {@code CapacityFullException} is thrown during insertion a list of
     * objects that were not inserted if returned
     * @param objects to be inserted to the bucket
     * @return null of the configuration for the split to be realized, if the node is full; the configuration
     *  contains the the list of not inserted objects
     * @throws AlgorithmMethodException 
     */
    @Override
    public SplitConfiguration insertObjects(List<LocalAbstractObject> objects) throws AlgorithmMethodException {
        SplitConfiguration splitConfiguration = super.insertObjects(objects);
        if (splitConfiguration == null || splitConfiguration.getNumberOfInsertedObjects() <= 0) {
            for (LocalAbstractObject object : objects) {
                updateInterval(object);
            }
        } else {
            for (LocalAbstractObject object : objects.subList(0, splitConfiguration.getNumberOfInsertedObjects())) {
                updateInterval(object);
            }
        }
        return splitConfiguration;
    }    
    
    /**
     * Sets the given storage to this node after a split was realized using the given split policy;
     *  also the new object count and key-interval must be set.
     * @param storage new data bucket
     * @param policy used split policy
     * @param partitionID id of the new partition from the split policy
     */
    @Override
    protected void setStorageAfterSplit(Bucket storage, SplitPolicyMIndexPartitioning policy, int partitionID) {
        super.setStorageAfterSplit(storage, policy, partitionID);
        this.coveredInterval = policy.getCoveredInterval(partitionID);
    }
    
    // *********************************    Update the key interval    *************************** //
        
    /**
     * Update the interval covered by this MIndexLeafCell
     * @param object that was inserted
     */
    private void updateInterval(LocalAbstractObject object) {
    	// update the object key on this level
        MIndexPP objectPP = MetricIndexes.readMIndexPP(object);
        objectPP.getClusterNumber(myKeyPP.getPPLevel());
        if (objectPP instanceof MIndexKey) {
            updateInterval((MIndexKey) objectPP);
        } 
    }

    private void updateInterval(MIndexKey addedKey) {
        if (coveredInterval.getFrom().compareTo(addedKey) > 0) {
            coveredInterval.setFrom(addedKey);
        } else if (coveredInterval.getTo().compareTo(addedKey) < 0) {
            coveredInterval.setTo(addedKey);
        }
    }
        
}
