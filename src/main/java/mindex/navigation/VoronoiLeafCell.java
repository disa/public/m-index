/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.operations.QueryOperation;
import mindex.MetricIndex;

/**
 * Abstract class encapsulates a leaf node of a cluster tree for a dynamic M-Index.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 * @param <O> type of data stored in the Voronoi leaf cell
 */
public abstract class VoronoiLeafCell<O extends LocalAbstractObject> extends VoronoiCell<O> {

    /** Class id for serialization. */
    private static final long serialVersionUID = 412001L;
    
    /** Number of objects stored in this leaf node. */
    protected volatile int objectCount = 0;
    //protected AtomicInteger objectCount = new AtomicInteger();
    
    /** If false this LeafCell was split and should not be used anymore. */
    private volatile VoronoiInternalCell<O> subtitutionOfThisLeaf = null;
    
    /**
     * Constructor given all parameters mandatory for the parent class. The storage and the covered interval are set to null.
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     */
    public VoronoiLeafCell(MetricIndex mIndex, VoronoiInternalCell parentNode) {
        super(mIndex, parentNode);
    }

    /**
     * Copy constructor.
     * @param copyFrom cell to copy the fields from (no clonning).
     */
    public VoronoiLeafCell(VoronoiLeafCell copyFrom) {
        super(copyFrom);
        this.objectCount = copyFrom.objectCount;
        this.subtitutionOfThisLeaf = copyFrom.subtitutionOfThisLeaf;
    }
    
    /**
     * Read the serialized disk storage from an object stream.
     * @param in the object stream from which to read the disk storage
     * @throws IOException if there was an I/O error during deserialization
     * @throws ClassNotFoundException if there was an unknown object in the stream
     */
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        // Proceed with standard deserialization first
        in.defaultReadObject();
    }    
    
    // ***********************      Abstract method implementation   ********************************* //
    
    @Override
    public int getObjectCount() {
        return objectCount;
    }

    @Override
    public int getObjectCountByObjects() {
        int i = 0;
        for (Iterator iterator = getAllObjects(); iterator.hasNext();) {
            iterator.next();
            i++;
        }
        return i;
    }
    
    @Override
    public short getLevel() {
        return (short) (parentNode.getLevel() + 1);
    }

    @Override
    public VoronoiLeafCell getResponsibleLeaf(short[] ppp, boolean createPath) throws AlgorithmMethodException {
        return this;
    }

    @Override
    public short[] getPppForReading() {
        return getParentNode().getChildPPP(this);
    }

    @Override
    public short[] getPppForReading(short thisIndex) {
        short[] retVal = Arrays.copyOf(parentNode.getPppForReading(), getLevel());
        retVal[retVal.length - 1] = thisIndex;
        return retVal;
    }
    
    // ***************************      Data and storage manipulation    ********************* //
        
    /**
     * Tries to insert all the objects into the bucket managed by this LeafCell.
     * If {@code CapacityFullException} is thrown during insertion a list of
     * objects that were not inserted if returned
     * @param objects to be inserted to the bucket
     * @return request for split, if the leaf became full during the insert
     * @throws AlgorithmMethodException 
     */
    public abstract SplitConfiguration insertObjects(List<O> objects) throws AlgorithmMethodException;
        
    /**
     * Method that deletes given objects from corresponding Bucket. If an instance
     * of MergeConfiguration is returned, the upper layer should merge this LeafCell (TODO).
     * @param objects to be deleted
     * @param deleteLimit maximal number of objects data-equal to given object that will be deleted from this node
     * @param checkLocator is true, the objects to be deleted are not located based on data but by locator; if locator
     *  of given object is <b>null</b>, the locator is ignored!!!
     * @return objects from {@code objects} that were NOT deleted
     * @throws AlgorithmMethodException 
     */
    public abstract Collection<O> deleteObjects(List<O> objects, int deleteLimit, boolean checkLocator) throws AlgorithmMethodException;

    
    /**
     * This method should be called only when the {@code insertObjects} method returns non-empty list of objects
     * (= {@code CapacityFullException} was throw during insertion of objects into bucket managed by this LeafCell)
     * or when the {@code checkAgileMultibucketPartitioning} returns true
     * @param splitConfiguration settings for the split operation
     * @return true, if the split was successfully accomplished
     * @throws messif.algorithms.AlgorithmMethodException if anything goes wrong
     */
    public abstract boolean split(SplitConfiguration splitConfiguration) throws AlgorithmMethodException;

    /**
     * Fallback to process given query operation on all data.
     * @param operation to process
     * @return the number of objects affected
     * @throws messif.algorithms.AlgorithmMethodException if anything goes wrong
     */
    public abstract int processOperation(QueryOperation operation) throws AlgorithmMethodException;
    
    // *******************************   Auxiliary  methods   **************************** //
    
    @Override
    public void readLock() {
        getParentNode().readLock();
    }
    
    @Override
    public void readUnLock() {
        getParentNode().readUnLock();
    }
    
    @Override
    public void writeLock() {
        getParentNode().writeLock();
    }
    
    @Override
    public void writeUnLock() {
        getParentNode().writeUnLock();
    }
    
    /**
     * Checks if this LeafCell was already split
     * @return true if this LeafCell is valid
     *         false if this LeafCell was split
     */
    public boolean isValid() {
        return subtitutionOfThisLeaf == null;
    }

    /**
     * If this cell is valid, this method returns null; otherwise, it returns
     * the new node that substituted this node in the Voronoi cell tree
     * structure.
     * @return the new node that substituted this node in the Voronoi cell tree; null, if this node is valid
     */
    public VoronoiInternalCell<O> getSubtitutionOfThisLeaf() {
        return subtitutionOfThisLeaf;
    }

    /**
     * If this leaf cell was split, then this method should be called to properly set the internal node that substitutes
     *  this node in the PPP-Tree.
     * @param newInternalSubstitute newly created internal cell
     */
    protected void invalidateThisNode(VoronoiInternalCell<O> newInternalSubstitute) {
        this.subtitutionOfThisLeaf = newInternalSubstitute;
    }
    
    @Override
    public String toString() {
        return new StringBuffer(": ").append( isValid() ).append(" bucket # ").append(" storing ").append(getObjectCount()).append("\n").toString();
    }

}
