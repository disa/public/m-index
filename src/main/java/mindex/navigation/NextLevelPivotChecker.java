/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

/**
 * Interface for objects (typically cells of PPP tree) that enable checking the existence of 
 *  specified pivot number on the next level of the PPP.
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface NextLevelPivotChecker {
    
    /**
     * Returns true, if this object (internal or leaf node) contains data with given pivot number.
     * @param pivotNumber
     * @return 
     */
    public boolean containsGivenPivot(int pivotNumber);
    
}
