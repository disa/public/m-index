/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import messif.algorithms.AlgorithmMethodException;
import mindex.MetricIndex;

/**
 * Encapsulates information about a super-cluster in the M-Index dynamic cluster hierarchy.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexPreciseInternalCell extends MIndexInternalCell {

    /** Class id for serialization */
    private static final long serialVersionUID = 41801L;

    
    /**
     * Create new object initializing cluster number, level and parentNode. The created node is empty - no child nodes are created!
     * @param mIndex M-Index logic
     * @param parentNode parent node of this node
     * @param pivotCombination the pivot combination corresponding to this cluster, e.g. [3,2,4] for cluster C_{3,2,4}
     * @throws AlgorithmMethodException if the cluster storage was not created successfully
     */
    public MIndexPreciseInternalCell(MetricIndex mIndex, MIndexPreciseInternalCell parentNode, short[] pivotCombination) throws AlgorithmMethodException {
        super(mIndex, parentNode, pivotCombination);
        //myKeyPP = new MIndexPPSimple(pivotCombination, mIndex.getMaxLevel(), mIndex.getNumberOfPivots());
    }


    // ************************   Sub-level manipulation  *********************************** //
    
    /**
     * Creates a specific child according to the type of this internal cell (data/nodata etc.) - in this case the Precise 
     *  variants of the nodes are always created {@link MIndexPreciseInternalCell} and {@link MIndexPreciseLeafCell}.
     * @param internalNode if to create an internal or leaf cell
     * @param newCombination PPP of the new node
     * @return newly created cell
     * @throws messif.algorithms.AlgorithmMethodException if the cell cannot be created
     */
    @Override
    public VoronoiCell createSpecificChildCell(boolean internalNode, short[] newCombination) throws AlgorithmMethodException {
        return internalNode ? new MIndexPreciseInternalCell(mIndex, this, newCombination) :
                new MIndexPreciseLeafCell(mIndex, this, newCombination);        
    }
    
}
