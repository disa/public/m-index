/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.navigation;

import mindex.distance.PartialQueryPPPDistanceCalculator;

/**
 * An interface that enables to revise a query-PPP calculated distance by a more demanding process 
 *  (for instance by reading all PP suffixes from leaf cell in PPP index.
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public interface MinQueryDistanceRevisor {
    
    public float reviseMinDistance(PartialQueryPPPDistanceCalculator calculator, float origDistance);
    
}
