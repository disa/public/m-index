/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import mindex.MetricIndex;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.NavigationProcessor;
import messif.objects.LocalAbstractObject;
import messif.operations.Approximate;
import messif.operations.Approximate.LocalSearchType;
import messif.operations.query.KNNQueryOperation;

/**
 * Implementation of {@link NavigationProcessor} that processes the {@link KNNQueryOperation}.
 * 
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class KNNProgressiveNavigationProcessor extends ApproxNavigationProcessor {

    /**
     * Creates new approximate navigation processor for given operation and M-Index. The priority queue is initialized.
     * @param operation approximate kNN operation (should be the same for approximate range)
     * @param mIndex M-Index object with configuration and dynamic cell tree
     * @throws AlgorithmMethodException if the M-Index cannot determine the PP for query object
     */
    public KNNProgressiveNavigationProcessor(KNNQueryOperation operation, MetricIndex mIndex) throws AlgorithmMethodException {
        super(operation, new ApproximationSettings(LocalSearchType.PERCENTAGE, 100), mIndex);
    }

    /** Class encapsulating the approximation settings */
    private static class ApproximationSettings implements Approximate {
        
        /** Type of the local approximation parameter used. */
        protected LocalSearchType localSearchType;
        
        /**
         * Value of the local approximation parameter. 
         * Its interpretation depends on the value of {@link #localSearchType}.
         */
        protected int localSearchParam;
        
        /** Radius for which the answer is guaranteed as correct.
         * It is specified in the constructor and can influence the level of approximation.
         * An algorithm evaluating this query can also change this value, so it can
         * notify about the guarantees of evaluation.
         */
        protected float radiusGuaranteed = LocalAbstractObject.UNKNOWN_DISTANCE;

        /**
         * Constructor with given search type and parameter.
         * @param localSearchType setter for {@link #localSearchType}
         * @param localSearchParam setter for {@link #localSearchParam} 
         */
        public ApproximationSettings(LocalSearchType localSearchType, int localSearchParam) {
            this.localSearchType = localSearchType;
            this.localSearchParam = localSearchParam;
        }
        
        @Override
        public LocalSearchType getLocalSearchType() {
            return localSearchType;
        }

        @Override
        public void setLocalSearchParam(int localSearchParam) {
            this.localSearchParam = localSearchParam;
        }

        @Override
        public void setLocalSearchType(LocalSearchType localSearchType) {
            this.localSearchType = localSearchType;
        }

        @Override
        public int getLocalSearchParam() {
            return localSearchParam;
        }

        @Override
        public void setRadiusGuaranteed(float radiusGuaranteed) {
            this.radiusGuaranteed = radiusGuaranteed;
        }

        @Override
        public float getRadiusGuaranteed() {
            return radiusGuaranteed;
        }
    }
}
