/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.NavigationProcessor;
import messif.buckets.Bucket;
import messif.operations.QueryOperation;
import mindex.navigation.VoronoiInternalCell;
import mindex.navigation.VoronoiLeafCell;

/**
 * Implementation of {@link NavigationProcessor} that processes any {@link QueryOperation}
 * on a set of StructureLeafNodes and their respective {@link Bucket}s.
 * The buckets where the operation should be processed is provided via constructor.
 * 
 * <h4>Parallelism</h4>
 * The implementation is thread-safe, if operation cloning is enabled. Otherwise,
 * the operation answer needs to be synchronized.
 * 
 * @param <O> the type of the operation that are processed by this navigator processor
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class CellQueryOpNavigationProcessor<O extends QueryOperation<?>> implements NavigationProcessor<O> {

    /** Operation for which this navigator was created */
    private final O operation;
    /** Internal queue of buckets to process */
    private final Queue<VoronoiLeafCell> leafNodes;
    /** Number of buckets processed so far */
    private int processed;
    /** Flag whether the queue is closed, if <tt>false</tt>, i.e. getting a next element from the queue blocks and waits for the queue to fill */
    private boolean queueClosed;
    
    /**
     * Create a new bucket navigation processor.
     * The processor is {@link #isClosed() closed} and contains only the specified buckets.
     * No additional buckets can be added.
     * @param operation the operation to process
     * @param leafNodes list of nodes to execute the operation on
     */
    public CellQueryOpNavigationProcessor(O operation, Collection<VoronoiLeafCell> leafNodes) {
        this.operation = operation;
        this.leafNodes = new LinkedList<>(leafNodes);
        this.queueClosed = true;
    }

    /**
     * Create a new bucket navigation processor.
     * The processor does not contain any buckets and the processing
     * will block in method {@link #processNext()}. Additional buckets must be
     * added via {@link #addBucket} methods and then {@link #closeQueue() () closed}
     * in order to be able to finish the processing.
     * 
     * @param operation the operation to process
     */
    public CellQueryOpNavigationProcessor(O operation) {
        this.operation = operation;
        this.leafNodes = new LinkedList<>();
        this.queueClosed = false;
    }

    /**
     * Adds a collection of leaf nodes to this processor.
     * Note that leaf nodes can be added only if this processor is not {@link #isClosed() closed}.
     * 
     * @param leaf the leaf node to add
     * @return if the passed leaf node is not valid any more (was split) then 
     *   this method returns the new node in the tree structure; null is returned otherwise
     * @throws IllegalStateException if this processor is already {@link #isClosed() closed}.
     */
    protected VoronoiInternalCell addLeafNode(VoronoiLeafCell leaf) throws IllegalStateException {
        leaf.readLock();
        // check whether the leaf node wasn't split in the meanwhile
        if (! leaf.isValid()) {
            leaf.readUnLock();
            return leaf.getSubtitutionOfThisLeaf();
        }
        synchronized (this.leafNodes) {
            if (queueClosed) {
                throw new IllegalStateException();
            }
            this.leafNodes.add(leaf);
            this.leafNodes.notifyAll();
        }
        return null;
    }

    /**
     * Closes this processor.
     * That means that no additional leaf nodes can be added and the {@link #processNext()}
     * method will block and wait for additional leaf nodes.
     * 
     * @throws IllegalStateException if this processor is already {@link #isClosed() closed}.
     */
    public void closeQueue() throws IllegalStateException {
        synchronized (leafNodes) {
            if (queueClosed) {
                throw new IllegalStateException();
            }
            queueClosed = true;
            leafNodes.notifyAll();
        }
    }

    /**
     * Returns whether additional leaf nodes can be added to this processor (<tt>false</tt>)
     * or this processor is closed (<tt>true</tt>).
     * @return <tt>true</tt> if this processor is closed and no additional leaf nodes can be added for processing
     */
    public boolean isClosed() {
        return queueClosed;
    }

    @Override
    public O getOperation() {
        return operation;
    }

    @Override
    public boolean isFinished() {
        return queueClosed && leafNodes.isEmpty();
    }

    /**
     * Polls next M-Index cell from the queue of cells to be processed (thread safe) and
     *  runs the processing by calling method {@link #processOnLeaf(mindex.navigation.MIndexLeafCell) }.
     * @return the number of objects affected
     * @throws java.lang.InterruptedException if the thread was interrupted during the processing
     */
    @Override
    public boolean processStep() throws InterruptedException, AlgorithmMethodException {
        VoronoiLeafCell leafNode;
        synchronized (leafNodes) {
            leafNode = leafNodes.poll();
            while (leafNode == null) {
                if (queueClosed) {
                    return false;
                }
                leafNodes.wait();
                leafNode = leafNodes.poll();
            }
        }
        processOnLeaf(operation, leafNode);
        leafNode.readUnLock();
        processed++;
        return true;
    }

    /**
     * This is default implementation of local processing of the operation on given M-Index leaf cell.
     *  It is called by {@link #processNext() } and can be override.
     * @param localOperation operation to process on the leaf
     * @param leafNode M-Index leaf cell to process the operation on
     * @return the number of objects affected
     * @throws messif.algorithms.AlgorithmMethodException if the operation processing goes wrong
     */
    protected int processOnLeaf(O localOperation, VoronoiLeafCell leafNode) throws AlgorithmMethodException {
        return leafNode.processOperation(localOperation);
    }
    
    @Override
    public int getProcessedCount() {
        return processed;
    }

    @Override
    public int getRemainingCount() {
        return leafNodes.size();
    }

    @Override
    public void close() {
        for (VoronoiLeafCell lockedLeaf : leafNodes) {
            lockedLeaf.readUnLock();
        }
        if (! operation.isFinished()) {
            operation.endOperation();
        }
    }
}

