/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.processors;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.impl.AbstractNavigationProcessor;
import messif.buckets.Bucket;
import messif.buckets.LocalBucket;
import messif.buckets.index.SearchAbstractObjectIterator;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.Approximate;
import messif.operations.Approximate.LocalSearchType;
import messif.operations.RankingSingleQueryOperation;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.statistics.OperationStatistics;
import mindex.MetricIndex;
import mindex.MetricIndexes;
import mindex.distance.PartialQueryPPPDistanceCalculator;
import mindex.distance.QueryPPPDistanceCalculator;
import mindex.navigation.MIndexLeafCell;
import mindex.navigation.MIndexPreciseLeafCell;
import mindex.navigation.MinQueryDistanceRevisor;
import mindex.navigation.VoronoiCell;
import mindex.navigation.VoronoiInternalCell;
import mindex.navigation.VoronoiLeafCell;
import mindex.processors.ApproxNavigationProcessor.RankableCell;

/**
 * Navigation processor for kNN or range query evaluated in approximate manner on M-Index.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class ApproxNavigationProcessor extends AbstractNavigationProcessor<RankingSingleQueryOperation, SinglePPPRankable> {
    
    /** Queue of M-Index leaf cells sorted by M-Index approx. penalty */
    protected final Queue<SinglePPPRankable> priorityQueue;
    
    /** Calculator of "distance" between the query and PPP */
    protected final QueryPPPDistanceCalculator queryPPPDistCalculator;
    
    /** Limit of the search costs to be spent by this context (and sub-contexts). */
    protected final ApproxSearchCosts predictedCosts;

    /** Statistics about the real costs spent. */
    protected final ApproxSearchCosts realCosts;
    
    /** Flag saying whether the key intervals should be checked for this approximate operation */
    protected final boolean checkKeyInterval;

    /** Flag saying whether the double-pivot filtering should be applied */
    protected final boolean checkDoubleFiltering;
    
    /** Counters of objects that were added to the priority queue */
    public volatile int nodesAddedToQ;

    /** Number of items processed from the queue */
    public volatile int queueItemsProcessed;
    
    /** Thread-safe cache, used when multi-buckets used and the flag "bucket.process_whole_multi_bucket" is true */
    protected MultiBucketCache cache;
    
    /** Set of accessed buckets to be checked when multi-buckets are used */
    protected final Set<Bucket> processedBuckets;

    /** the M-Index object */
    protected final MetricIndex mIndex;

    /**
     * Creates new approximate navigation processor for given operation and M-Index. The priority queue is initialized.
     * @param operation approximate operation - typically kNN or range (should be the same for approximate range)
     * @param approxSettings the approximate settings for the operation (typically it can be the same instance as operation)
     * @param mIndex M-Index object with configuration and dynamic cell tree
     * @throws AlgorithmMethodException if the M-Index cannot determine the PP for query object
     */
    public ApproxNavigationProcessor(RankingSingleQueryOperation operation, Approximate approxSettings, MetricIndex mIndex) throws AlgorithmMethodException {
        //this(operation, ApproxSearchCosts.getMIndexApproxSearchCosts(approxSettings, mIndex), mIndex, new SortedCollection<>(pPPComparator), true);
        this(operation, ApproxSearchCosts.getMIndexApproxSearchCosts(approxSettings, mIndex), mIndex, new PriorityQueue(1000, pPPComparator), true);
    }  
    
    protected ApproxNavigationProcessor(RankingSingleQueryOperation operation, ApproxSearchCosts approxCosts, MetricIndex mIndex, Queue<SinglePPPRankable> queue, boolean checkDoubleFiltering) throws AlgorithmMethodException {
        super(operation, false, false, queue, false);
        this.mIndex = mIndex;
        priorityQueue = queue;
        queryPPPDistCalculator = MetricIndexes.getQueryPPPDistCalculator(operation, mIndex);
        checkKeyInterval = mIndex.isApproxCheckKeyInterval() && mIndex.isPreciseSearch();
        this.checkDoubleFiltering = checkDoubleFiltering;
        
        cache = (mIndex.isMultiBuckets() && ! mIndex.isApproxProcessWholeMultiBucket()) ? new MultiBucketCache() : null;
        processedBuckets = (mIndex.isMultiBuckets() && mIndex.isApproxProcessWholeMultiBucket()) ? new HashSet() : null;
        priorityQueue.add(new RankableCell(mIndex.getVoronoiCellTree(), queryPPPDistCalculator));
        nodesAddedToQ = 1;

        try {
            predictedCosts = approxCosts;
            if (predictedCosts != null) {
                predictedCosts.updateSingleCosts(LocalSearchType.ABS_DC_COUNT, (int) OperationStatistics.getOpStatisticCounter("DistanceComputations").get());
                realCosts = predictedCosts.clone();
            } else {
                realCosts = null;
            }
        } catch (CloneNotSupportedException ex) {
            MetricIndexes.logger.log(Level.SEVERE, null, ex);
            throw new AlgorithmMethodException(ex);
        }
    }
    
    /** 
     * This method finds the next leaf cell in the priority queue and sets it to {@link #nextBestCell}. Filtering is applied
     * @param isAsynchronous true, if the processing using this processor is currently asynchronous
     * @return the next {@code RankableCell} to be processed, or null if the top item is NOT instance of {@link RankableCell} (or if queue is empty)
     */
    @Override
    protected synchronized RankableCell getNextProcessingItem(boolean isAsynchronous) {
        // check the stop condition, if to be checked; 
        if ((isAsynchronous ? predictedCosts : realCosts) != null && (!(isAsynchronous ? predictedCosts : realCosts).checkCostCondition())) {
            priorityQueue.clear();
            return null;
        }

        SinglePPPRankable head;
        while ((head = priorityQueue.peek()) != null && (head instanceof RankableCell)) {
            RankableCell bestCell = (RankableCell) priorityQueue.poll();
            if (bestCell.reviseDistance(queryPPPDistCalculator)) {
                priorityQueue.add(bestCell);
                continue;
            }
            queueItemsProcessed ++;
            if (checkDoubleFiltering && filterByDoublePivot(bestCell.cell, bestCell.getPPP(), getOperation().getAnswerThreshold())) {
                continue;
            }
            VoronoiLeafCell bestLeaf;
            if (bestCell.cell instanceof VoronoiLeafCell || bestCell.cell.getLevel() >= queryPPPDistCalculator.getMaxLevel()) {
//                if (bestCell.cell.getObjectCount() <= 0) {
//                    continue;
//                }
                bestCell.cell.readLock();
                if (bestCell.cell instanceof VoronoiLeafCell) {
                    bestLeaf = (VoronoiLeafCell) bestCell.cell;
                    // check whether the leaf node wasn't split in the meanwhile
                    if (! bestLeaf.isValid()) {
                        priorityQueue.add(new RankableCell(bestLeaf.getSubtitutionOfThisLeaf(), bestCell.rankingDistance, bestCell.thisIndex));
                        nodesAddedToQ++;
                        bestLeaf.readUnLock();
                        continue;
                    }
                    if (predictedCosts != null) {
                        predictedCosts.updatePredictMaxCosts(bestLeaf);
                    }
                    // if the leaf storage was already processed, ignore it
                    if (processedBuckets != null && processedBuckets.contains(((MIndexLeafCell) bestLeaf).getStorage())) {
                        bestLeaf.readUnLock();
                        continue;
                    }
                }
                return bestCell;
            }
            // generate the contexts for all subnodes
            for (Iterator<Map.Entry<Short, VoronoiCell<? super LocalAbstractObject>>> childNodes = ((VoronoiInternalCell) bestCell.cell).getChildNodes(); childNodes.hasNext(); ) {
                Map.Entry<Short, VoronoiCell<? super LocalAbstractObject>> entry = childNodes.next();
                priorityQueue.add(new RankableCell(entry.getValue(), queryPPPDistCalculator, bestCell, entry.getKey()));
                nodesAddedToQ ++;
            }
        }
        return null;
    }
    
    @Override
    protected RankingSingleQueryOperation processItem(RankingSingleQueryOperation operation, SinglePPPRankable processingItem) throws AlgorithmMethodException {
        try {
            // evaluate the operation on relevant data
            long currentDCCount = OperationStatistics.getOpStatisticCounter("DistanceComputations").get();
            if (! (((RankableCell) processingItem).cell instanceof MIndexLeafCell)) {
                throw new AlgorithmMethodException("this approx. operation can be processed only on MIndexLeafCellData");
            }
            MIndexLeafCell leafCell = (MIndexLeafCell) ((RankableCell) processingItem).cell;
            int accessedObjects = processOnLeaf(operation, leafCell, ((RankableCell) processingItem).getPPP());

            if (realCosts != null) {
                realCosts.updateAfterSearch(accessedObjects, (int) (OperationStatistics.getOpStatisticCounter("DistanceComputations").get() - currentDCCount));
            }
            return operation;
        } finally {
            ((RankableCell) processingItem).cell.readUnLock();
        }
    }
    
    /**
     * Process the approximate query on the data from given M-Index Voronoi leaf cell 
     *  (call standard {@link ApproxKNNQueryOperation#evaluate(messif.objects.util.AbstractObjectIterator) } on relevant data).
     * @param operation query operation to be processe on the leaf
     * @param leaf M-Index Voronoi leaf cell
     * @return number of objects processed (accessed) by the operation
     * @throws AlgorithmMethodException if the evaluate method fails
     */
    protected int processOnLeaf(RankingSingleQueryOperation operation, MIndexLeafCell leaf, short [] leafPPP) throws AlgorithmMethodException {
        if (leaf.getStorage() == null) {
            return 0;
        }
        float radius = operation.getAnswerThreshold();

        // if there are multibuckets and whole multibuckets are always processed
        if (processedBuckets != null) {
            synchronized (processedBuckets) {
                // if this bucket was already fully processed
                if (! processedBuckets.add(leaf.getStorage())) {
                    return 0;
                }
            }
        } 
        
        // try to filter the leaf out by multiple application of Double-pivot constraint (and Pivot-Range constraint)
        if (filterOut(leaf, leafPPP, radius)) {
            return 0;
        }

        if (processedBuckets != null) {
            leaf.getStorage().processQuery(operation);
            OperationStatistics.getOpStatisticCounter("AccessedBuckets").add();
            return ((LocalBucket) leaf.getStorage()).getObjectCount();
        }
        
        //  if whole buckets are always processed
        if (!(leaf.getStorage() instanceof LocalBucket)) {
            leaf.processOperation(operation);
            OperationStatistics.getOpStatisticCounter("AccessedBuckets").add();
            return leaf.getObjectCount();
        }
        
        // get the iterator 
        int accessedObjects = -1;
        AbstractObjectIterator<LocalAbstractObject> iterator;
        
        // use the interval            
        if (checkKeyInterval) {
            iterator = RangeNavigationProcessor.getIntervalIterator(leaf, 
                    RangeNavigationProcessor.createRelevantInterval((MIndexPreciseLeafCell) leaf, radius, queryPPPDistCalculator.getQueryPivotDistances(), mIndex), cache, mIndex);
        } else {
            if (mIndex.isMultiBuckets() && cache != null) {
                // for pure disk buckets ignore the limits completely
                iterator = cache.processBucket(leaf.getStorage(), leaf).iterator();
            } else {
                OperationStatistics.getOpStatisticCounter("AccessedBuckets").add();
                iterator = leaf.getAllObjects();
            }
            accessedObjects = leaf.getObjectCount();
        }
        
        // evaluate the operation on relevant data
        operation.evaluate(iterator);
        
        // update the search costs
        if (accessedObjects < 0) {
            accessedObjects = ((SearchAbstractObjectIterator) iterator).getCount();
        }
        return accessedObjects;
    }

    @Override
    public void close() {
        if (realCosts != null) {
            realCosts.setThreadStatistics();
        }
        if (cache != null) {
            OperationStatistics.getOpStatisticCounter("RedundantReadObjects").set(cache.getCurrentObjectCount());
            OperationStatistics.getOpStatisticCounter("RedundantReadCells").set(cache.getCurrentCellCount());
        }
        if (! getOperation().isFinished()) {
            getOperation().endOperation();
        }
        super.close();
    }
    
    /**
     * This method checks whether the specified cluster can be filtered out using repetitive application of double-pivot distance constraint.
     * @param node M-Index Voronoi cell
     * @param radius actual query radius
     * @return true only if given node can be filtered out according to given query and radius
     */
    protected boolean filterByDoublePivot(VoronoiCell node, short [] nodePPP, float radius) {
        return MetricIndexes.filterOutCluster(queryPPPDistCalculator.getQueryPivotDistances(), queryPPPDistCalculator.getQueryPPP(), nodePPP, radius);
    }

    /**
     * This method tries to filter out the leaf cell by repetitive application of double-pivot constraint {@link #filterByDoublePivot(mindex.navigation.MIndexCell, float) }
     *   and by range-pivot constraint, if the interval is available.
     * @param leaf leaf Voronoi cell to filter out
     * @param radius actual query radius
     * @return true, if given cell can be filtered out by the two mentioned constraints
     */
    protected boolean filterOut(MIndexLeafCell leaf, short [] leafPPP, float radius) {
        // if radius of the query is already set, try to filter the bucket out using multiple double-pivot constraint
        if (filterByDoublePivot(leaf, leafPPP, radius)) {
            return true;
        }
        // try to filter using range-pivot distance constraint
        if (leaf instanceof MIndexPreciseLeafCell) {
            if (! ((MIndexPreciseLeafCell) leaf).getCoveredInterval().intersect(RangeNavigationProcessor.createRelevantInterval((MIndexPreciseLeafCell) leaf, radius, queryPPPDistCalculator.getQueryPivotDistances(), mIndex))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Internal class used to compare approx context to order contexts increasingly.
     */
    protected static class RankableCell implements SinglePPPRankable {
        
        private static final float EQUALITY_TOLERANCE = 0.0001f;
        
        /** M-Index penalty of the cluster - distance from current query object */
        private float rankingDistance;

        /** M-Index penalty of the cluster - distance from current query object */
        private final float ownQueryDistance;
        
        /** M-Index cluster */
        protected final VoronoiCell cell;
        
        /** This cells last PP index (useful for leaf cells) */
        protected final short thisIndex;
        
        /** Flag saying if the distance can be revised or not */
        protected boolean wasRevised = false;

        @Override
        public float getQueryDistance() {
            return rankingDistance;
        }
        
        public VoronoiCell getCell() {
            return cell;
        }

        public final short[] getPPP() {
            return cell.getPppForReading(thisIndex);
        }

        public float getOwnQueryDistance() {
            return ownQueryDistance;
        }
        
        /** Sets the cell and calculates approximate penalty */
        public RankableCell(final VoronoiCell cell, QueryPPPDistanceCalculator calculator, RankableCell topCell, short thisIndex) {
            this.cell = cell;
            this.thisIndex = thisIndex;
            if (calculator instanceof PartialQueryPPPDistanceCalculator) {
                this.rankingDistance = topCell.ownQueryDistance + ((PartialQueryPPPDistanceCalculator) calculator).getPartialQueryDistance(thisIndex, topCell.cell.getLevel() + 1);
            } else {
                this.rankingDistance = calculator.getQueryDistance(getPPP());
            }
            this.ownQueryDistance = this.rankingDistance;
        }

        public RankableCell(final VoronoiCell rootCell, QueryPPPDistanceCalculator calculator) {
            this.cell = rootCell;
            this.ownQueryDistance = calculator.getQueryDistance(cell.getPppForReading());
            this.rankingDistance = ownQueryDistance;
            this.thisIndex = (short) 0;
        }

        public RankableCell(final VoronoiCell cell, float distance, short thisIndex) {
            this.cell = cell;
            this.ownQueryDistance = distance;
            this.rankingDistance = ownQueryDistance;
            this.thisIndex = thisIndex;
        }
        
        protected boolean reviseDistance(QueryPPPDistanceCalculator calculator) {
            try {
                if (!(calculator instanceof PartialQueryPPPDistanceCalculator && cell instanceof MinQueryDistanceRevisor && !wasRevised)) {
                    return false;
                }
                this.rankingDistance = ((MinQueryDistanceRevisor) cell).reviseMinDistance(((PartialQueryPPPDistanceCalculator) calculator), ownQueryDistance);
                if (ownQueryDistance > rankingDistance + EQUALITY_TOLERANCE) {
                    MetricIndexes.logger.warning("original distance was larger than the revised: " + ownQueryDistance + ", " + rankingDistance + ", " + cell.getPppForReading());
                }
                return (ownQueryDistance < rankingDistance);
            } finally {
                wasRevised = true;
            }
        }
        
    };
    
    /**
     * Comparator between objects or M-Index cells according to query-PPP distance.
     */
    public static Comparator<SinglePPPRankable> pPPComparator = new Comparator<SinglePPPRankable>() {

        @Override
        public int compare(SinglePPPRankable o1, SinglePPPRankable o2) {
            return Float.compare(o1.getQueryDistance(), o2.getQueryDistance());
        }
    };
    
}
