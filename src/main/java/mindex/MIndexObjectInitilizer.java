/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFilter;
import messif.objects.PrecomputedDistancesFixedArrayFilter;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import mindex.processors.ApproxSearchCosts;

/**
 * This is an abstract class carries part of the M-Index configuration that 
 *   deals with pivots and calculation of distances between object and pivots.
 * It can especially create the M-Index pivot permutation (M-Index key).
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public abstract class MIndexObjectInitilizer implements Serializable {

    /** Class serial ID for serialization */
    private static final long serialVersionUID = 112080L;
    
    /** The whole M-Index configuration is kept in the extended properties file */
    protected final MIndexProperties configuration;
    
    /** The set of pivots are fixed during the whole life of the index - can be null. */
    protected final LocalAbstractObject[] pivots;    
    
    /** Max distance of the object class */
    protected final float maxDistance;

    
    // ************************   Transient fields (depending on the actual configuration)  ***************** //

    /** Maximal M-Index partitioning level */
    protected transient short maxLevel;
    
    /** Size of the pivots set */
    protected transient long maxNumberOfClusters;
    
    /** Number of pivots - it can depend on several aspects. */
    protected transient short numberOfPivots;
    
    /** Matrix of mutual pivot distances */
    protected transient float[][] pivotDistancesMatrix = null;

    
    // ****************************       Initialization      ************************************* //
    
    /**
     * Creates the PP calculator given M-Index properties
     * @param mIndexProperties properties with M-Index configuration
     * @throws InstantiationException if this part of M-Index cannot be initialized using this properties
     */
    public MIndexObjectInitilizer(MIndexProperties mIndexProperties) throws InstantiationException {
        this.configuration = mIndexProperties;
        // handle the pivots - either read them from the file or only the number is specified
        this.pivots = (configuration != null) ? initPivots(configuration): null;
        this.maxDistance = (pivots != null) ? pivots[0].getMaxDistance() : LocalAbstractObject.MAX_DISTANCE;
        
        if (configuration != null) {
            setPrecomputedFields();
        }
    }        

    /** 
     * Given maximal specified number of pivots and configuration property file, this method
     *  returns the prepared list of pivot objects.
     * @param configuration M-Index configuration
     * @return the prepared list of pivot objects or <b>null</b> if file does not exist or is empty
     */
    public static LocalAbstractObject[] initPivots(MIndexProperties configuration) {
        try {
            short nMaxPivots = configuration.getShortProperty(MIndexProperty.NUMBER_OF_PIVOTS);
            if (! new File(configuration.getProperty(MIndexProperty.PIVOT_FILE)).canRead()) {
                return null;
            }
            // read the pivots from file
            StreamGenericAbstractObjectIterator<LocalAbstractObject> iterator = new StreamGenericAbstractObjectIterator<>(getObjectClass(configuration), configuration.getProperty(MIndexProperty.PIVOT_FILE));
            AbstractObjectList<LocalAbstractObject> pivotsList = (nMaxPivots >= 0) ? 
                    new AbstractObjectList<>(iterator, nMaxPivots) : new AbstractObjectList<>(iterator);
            
            if (pivotsList.size() <= 0) {
                return null;
            }
            return pivotsList.toArray(new LocalAbstractObject[0]);
        } catch (IOException io) {
            return null;
        }
    }
        
    /**
     * Deserialization method - update the precomputed fields.
     * @param in stream
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        try {
            if (configuration != null) {
                setPrecomputedFields();
            }
        } catch (InstantiationException ex) {
            throw new IOException(ex);
        }
    }
    
    /**
     * This method sets all the transient fields - it is called from the constructor 
     *  and when the object is being deserialized.
     */
    private void setPrecomputedFields() throws InstantiationException {
        this.maxLevel = configuration.getShortProperty(MIndexProperty.MAX_LEVEL);
        if (pivots != null) {
            this.numberOfPivots = (short) pivots.length;

            // calculate and set average pivot disk size
            int avgSize = 0;
            for (LocalAbstractObject localAbstractObject : pivots) {
                avgSize += localAbstractObject.getSize();
            }
            avgSize /= pivots.length;
            
            // calculate the serialization size of the precomputed distances (ignore size of PPP)
            int headerSize = isUsePivotFiltering() ? (numberOfPivots * (Float.SIZE / 8)) : 0; //: (getMaxLevel() * (Integer.SIZE / 8) + 8));
            ApproxSearchCosts.setObjectDiskSize(avgSize + headerSize);
            MetricIndexes.logger.log(Level.FINE, "object size: {0}, with precomp. dists.: {1}", new Object[]{avgSize, ApproxSearchCosts.getObjectDiskSize()});
        } else {
            this.numberOfPivots = (short) configuration.getShortProperty(MIndexProperty.NUMBER_OF_PIVOTS);            
        }
        
        if (numberOfPivots <= 0) {
            throw new InstantiationException("The number of pivots is not positive.");
        }
        
        // if the maximal number of clusters should be larger than Long.MAX then print warning (not fatal for PPP-Codes)
        if (Math.pow(numberOfPivots, maxLevel) > Long.MAX_VALUE) {
            MetricIndexes.logger.log(Level.SEVERE, "having {0} pivots and max level {1}, the theoretical number of clusters exceeds the Long.MAX (not fatal for PPP-Codes)", 
                    new Object [] { getNumberOfPivots(), getMaxLevel()});
            this.maxNumberOfClusters = Long.MAX_VALUE;
        //while (Math.pow(getNumberOfPivots(), getMaxLevel()) > Long.MAX_VALUE && !configuration.getBoolProperty(MIndexProperty.PPP_CODES)) {
            //configuration.setProperty(MIndexProperty.MAX_LEVEL, (short) (getMaxLevel() - 1));
            //MetricIndexes.logger.log(Level.WARNING, "lowering max level to: {0}", getMaxLevel());
        } else {
            this.maxNumberOfClusters = (long) Math.pow(numberOfPivots, maxLevel);
        }
    }

    private void initPivotDistMatrix() {
        // init the matrix of pivot-query distances
        this.pivotDistancesMatrix = new float[numberOfPivots][numberOfPivots];
        for (int i = 0; i < numberOfPivots; i++) {
            for (int j = 0; j < i; j++) {
                this.pivotDistancesMatrix[i][j] = getPivot(i).getDistance(getPivot(j));
                this.pivotDistancesMatrix[j][i] = this.pivotDistancesMatrix[i][j];
            }
            this.pivotDistancesMatrix[i][i] = 0f;
        }        
    }
    
    // ****************************       Getters      ************************************* //
    
    /** 
     * Getter for individual pivots.
     * @param pivotNumber pivot number to get [0-(n-1)]
     * @return a pivot given a number
     */
    public LocalAbstractObject getPivot(int pivotNumber) {
        if (pivots == null) {
            return null;
        }
        return pivots[pivotNumber];
    }

    /**
     * Returns distance between given two pivots, if the pivots are set; it is 
     *  precomputed in a pivot x pivot matrix.
     * @param pivot1 the first pivot index
     * @param pivot2 the second pivot index
     * @return distance between the pivots or -1 (if pivots not set)
     */
    public float getPivotsDistance(int pivot1, int pivot2) {
        if (pivots == null)
            return -1f;
        if (pivotDistancesMatrix == null) {
            initPivotDistMatrix();
        }
        return this.pivotDistancesMatrix[pivot1][pivot2];
    }
    
    
    /** Number of pivots (it IS always known). */
    public short getNumberOfPivots() {
        return numberOfPivots;
    }

    /** Returns the maximal number of clusters created by this M-Index: $(number-of-pivots)^level$ */
    public long getMaxNumberOfClusters() {
        return this.maxNumberOfClusters;
    }
    
    /** Return the maximal distance of given metric space (if the pivots are set). */
    public float getMaxDistance() {
        return maxDistance;
    }
    
    public boolean isUsePivotFiltering() {
        return configuration.getBoolProperty(MIndexProperty.PIVOT_FILTERING);
    }

    public boolean isCheckDuplicateDC() {
        return configuration.getBoolProperty(MIndexProperty.CHECK_DUPLICATE_DC);
    }
    
    public boolean isUseExistingPP() {
        return configuration.getBoolProperty(MIndexProperty.USE_EXISTING_PP);
    }

    public boolean isPreciseSearch() {
        return configuration.getBoolProperty(MIndexProperty.PRECISE_SEARCH);
    }

    /** Return the maximal level of the M-Index. */
    public short getMaxLevel() {
        return maxLevel;
    }
    
    /** Return the class of this M-Index. */
    public final Class<? extends LocalAbstractObject> getObjectClass() {
        return MIndexObjectInitilizer.getObjectClass(configuration);
    }

    public static Class<? extends LocalAbstractObject> getObjectClass(MIndexProperties configuration) {
        return configuration.getClassProperty(MIndexProperty.DATA_CLASS.getKey(), true, LocalAbstractObject.class);
    }
    
    
    // ***************       Init object - calculate obj-pivot distance + PP  *********************  //
    
    /**
     * Initialize the object in M-Index: calculate all distances, sort them, set pivot permutation (and set
     *   filter, if the filter should be stored by this M-Index).
     * @param object object to initialize
     * @return the M-Index PP initialized by this method
     * @throws messif.algorithms.AlgorithmMethodException if anything goes wrong
     */
    public abstract LocalAbstractObject initPP(LocalAbstractObject object) throws AlgorithmMethodException;

    /**
     * Initialize the object in M-Index: calculate all distances, sort them, set pivot permutation (and set
     *   filter, if the filter should be stored by this M-Index).
     * @param object object to initialize
     * @return distances to pivots
     * @throws messif.algorithms.AlgorithmMethodException if anything goes wrong
     */
    public abstract float [] initPPGetDistances(LocalAbstractObject object) throws AlgorithmMethodException;
    
    /**
     * Initializes the PP of a collection of objects
     * @param objects the collection of objects to initialize the PP for
     * @return collection of initialized objects - it may differ from the passed one
     * @throws messif.algorithms.AlgorithmMethodException if anything goes wrong
     */
    public abstract Collection<? extends LocalAbstractObject> initPP(Collection<? extends LocalAbstractObject> objects) throws AlgorithmMethodException;
    
    
    //  ******************    Internal methods to be exploited by the abstract methods    ***************  //
    
    /**
     * Initialize the object in M-Index: calculate all distances, sort them, set pivot simple permutation (if precise search
     *  is not ON) or full M-Index key (if precise search is ON) and set filter (if filtering required).
     * After calling this method, the {@code object} has only one MIndexPP and that is the one created here.
     * @param object object to initialize
     * @param getDistances if the distances should be returned, independent of the pivot filtering usage
     * @return the object-pivot distances, or null if the {@code getDistances} is false
     * @throws AlgorithmMethodException if the distances have to be evaluated and pivots not set
     */
    protected float [] initObject(LocalAbstractObject object, boolean getDistances) throws AlgorithmMethodException {
        float [] retVal = null;        
        MIndexPP pivotPermutation = MetricIndexes.readMIndexPP(object);
        
        // try to use existing MIndexObjectPivotPermutation
        while (pivotPermutation != null && ! isUseExistingPP()) {
            object.unchainFilter((PrecomputedDistancesFilter) pivotPermutation);
            pivotPermutation = MetricIndexes.readMIndexPP(object);
        }
        
        if (pivotPermutation == null || getDistances) {
            retVal = getObjectPivotDistances(object);
        }

        if (pivotPermutation == null && retVal == null) {
            throw new AlgorithmMethodException("pivot permutation for this object cannot be determined (pivots are null and correct values were not passed): " + object);
        }
        
        // **************************   create filters and keys    ****************************** //
        if (isCheckDuplicateDC()) {
            object.chainFilter(new DuplicateDCFilter(object.getLocatorURI()), false);
        }
        
        if (isUsePivotFiltering()) {
            PrecomputedDistancesFixedArrayFilter filter = new PrecomputedDistancesFixedArrayFilter(object);
            filter.setFixedPivotsPrecompDist(retVal);
        }
        
        // create and set MIndexPP, if it was not set already
        if (pivotPermutation == null) {
            if (isPreciseSearch()) {
                short[] ppp = sortDistances(retVal, maxLevel);
                object.chainFilter(new MIndexKeyPPFull(ppp, maxLevel, numberOfPivots, retVal[ppp[0]] / maxDistance, (short) 0), false);
            } else {
                object.chainFilter(new MIndexPPSimple(sortDistances(retVal, maxLevel), maxLevel, numberOfPivots), false);
            }
        }
        
        return retVal;
    }
    
    
    /**
     * Initialize the pivot permutation prefix for given object. If the MIndexPP is already stored in the
     *  object (as MIndexPP) then return this; if the max level of the stored MIndexPP is not equal to {@link #maxLevel} then
     *  the MIndexPP is removed!
     * @param object object to initialize
     * @return PPP for given object with respect to pivots in this PP calculator
     * @throws AlgorithmMethodException if the distances have to be evaluated and pivots not set
     */
    protected short [] getObjectPPP(LocalAbstractObject object) throws AlgorithmMethodException {
        MIndexPP existingPP = MetricIndexes.readMIndexPP(object);
        
        // try to use existing MIndexObjectPivotPermutation
        while (existingPP != null && (! isUseExistingPP() || existingPP.getMaxLevel() != maxLevel)) {
            object.unchainFilter((PrecomputedDistancesFilter) existingPP);
            existingPP = MetricIndexes.readMIndexPP(object);
        }
        
        if (existingPP != null) {
            return existingPP.getPivotPermutation();
        }
        return sortDistances(getObjectPivotDistances(object), maxLevel);
    }
    
    
    /******************  Sorting methods  *****************/
    
    /**
     * Sort the current array creating the {@link #sortingIndexes} array of indexes and return it.
     * @param distances the distance array to be sorted
     * @param maxLevel keep only a given prefix of the sorting indexes
     * @return array of sorting indexes (pivot permutation of given length)
     */
    public static short[] sortDistances(float [] distances, short maxLevel) {
        DistanceIndexPair[] actualValues = new DistanceIndexPair[distances.length];
        for (short i = 0; i < distances.length; i++) {
            actualValues[i] = new DistanceIndexPair(distances[i], i);
        }
        Arrays.sort(actualValues);

        short [] sortingIndexes = new short[maxLevel];
        for (int i = 0; i < maxLevel; i ++ ) {
            sortingIndexes[i] = actualValues[i].index;
        }
        return sortingIndexes;
    }
    
    /**
     * Auxiliary class for efficient sorting of the distances and determining the sorting indexes.
     */
    protected static class DistanceIndexPair implements Comparable {

        protected final float distance;
        
        protected final short index;

        public DistanceIndexPair(float distance, short index) {
            this.distance = distance;
            this.index = index;
        }                
        
        @Override
        public int compareTo(Object o) {
            return Float.compare(distance, ((DistanceIndexPair)o ).distance);
        }
    }
    
    /**
     * This method returns the number of cluster corresponding to this PP an level; 
     *  it also depends on the maxlevel (size of PP).
     * @param pivotPermutation PP to create the long Key value from
     * @param level of the desired long key (1-based)
     * @return M-Index number of corresponding cluster
     */
    protected long computeClusterNumber(short [] pivotPermutation, short level) {
        long retVal = 0L;
        long constant = 1L;
        for (int i = maxLevel - 1; i >= 0; i--) {
            if (i < level && i < pivotPermutation.length) {
                retVal += pivotPermutation[i] * constant;
            }
            constant *= numberOfPivots;
        }
        return retVal;
    }
        
    /**
     * Either get or create the PrecomputedDistancesFixedArrayFilter for given object. If the flag {@link MetricIndex#usePivotFiltering} 
     *  is true, this method adds the filter to the object filter chain.
     * 
     * @param object data object to create filter for
     * @return the PrecomputedDistancesFixedArrayFilter for given object
     * @throws java.lang.IllegalArgumentException
     * @throws java.lang.NullPointerException
     */
    protected float [] getObjectPivotDistances(LocalAbstractObject object) throws IllegalArgumentException, NullPointerException, AlgorithmMethodException {
        PrecomputedDistancesFixedArrayFilter existing = object.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class);
        if (existing != null) {
            if (isUseExistingPP() && (existing.getPrecompDistSize() == numberOfPivots)) {
                return existing.getPrecompDist();
            } else {
                // remove wrong pivot filter
                object.unchainFilter(existing);
            }
        }
           
        if (pivots == null) {
            return null;
        }
        
        float[] distances = new float[numberOfPivots];
        for (int distanceIndex = 0; distanceIndex < numberOfPivots; distanceIndex++) {
            distances[distanceIndex] = object.getDistance(pivots[distanceIndex]);
        }

        return distances;
    }
    

    @Override
    public String toString() {
        StringBuilder strBuf = new StringBuilder();
        strBuf.append("max dist. = ").append(maxDistance).
                append("; max # of clusters = ").append(maxNumberOfClusters).append("; # of pivots = ").append(numberOfPivots).append(": \n");
        if (pivots == null) {
            strBuf.append("pivot set NOT SET.");
        } else {
            for (int i = 0; i < numberOfPivots; i++) {
                strBuf.append(getPivot(i).getLocatorURI()).append(", ");
            }
        }
        return strBuf.toString();
    }    
    
}
