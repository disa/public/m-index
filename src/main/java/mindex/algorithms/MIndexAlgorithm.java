/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.algorithms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.NavigationDirectory;
import messif.algorithms.NavigationProcessor;
import messif.buckets.LocalBucket;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.query.GetAlgorithmInfoOperation;
import messif.operations.query.GetAllObjectsQueryOperation;
import messif.operations.query.GetObjectCountOperation;
import mindex.MIndexObjectInitilizer;
import mindex.MetricIndex;
import mindex.MetricIndexes;

/**
 * This is the main M-Index centralized algorithm.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexAlgorithm extends Algorithm implements NavigationDirectory {

    /** Class serial id for serialization. */
    private static final long serialVersionUID = 20202L;    
    
    /** This object encapsulates the M-Index logic, configuration, storage, navigation. */
    protected final MetricIndex mIndex;
    
    /** A simple lock for periodic algorithm serialization. */
    protected transient Lock serializingLock = new ReentrantLock();

    /**
     * Creates the M-Index algorithm given the file name file name containing M-Index properties for new M-Index.
     *
     * @param properties text file with M-Index properties settings
     * @param prefix prefix (e.g. "mindex.") for items in property file that should be considered (the rest is ignored)
     * @throws java.lang.InstantiationException if the file does not contain what it should contain
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor from a created properties object", arguments = {"M-Index properties", "prefix"})
    public MIndexAlgorithm(Properties properties, String prefix) throws InstantiationException, AlgorithmMethodException {
        this("M-Index", properties, prefix);
    }

    /**
     * Creates the M-Index algorithm given the file name file name containing M-Index properties for new M-Index.
     *
     * @param name algorithm name 
     * @param properties text file with M-Index properties settings
     * @param prefix prefix (e.g. "mindex.") for items in property file that should be considered (the rest is ignored)
     * @throws java.lang.InstantiationException if the file does not contain what it should contain
     */
    @Algorithm.AlgorithmConstructor(description = "Constructor from a created properties object", arguments = {"Algorithm name", "M-Index properties", "prefix"})
    public MIndexAlgorithm(String name, Properties properties, String prefix) throws InstantiationException, AlgorithmMethodException {
        super(name);
        this.mIndex = new MetricIndex(properties, prefix);
        MetricIndexes.logger.info(mIndex.toString());
    }

    /**
     * Creates the algorithm given name and already created MIndex configuration instance.
     * @param name alg. name
     * @param mIndex created MIndex configuration object
     */
    protected MIndexAlgorithm(String name, MetricIndex mIndex) {
        super(name);
        this.mIndex = mIndex;
        MetricIndexes.logger.info(mIndex.toString());
    }
    
    /** Deserialize and set the transient fields. */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        serializingLock = new ReentrantLock();
    }

    /** Clean up everything */
    @Override
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    public void finalize() throws Throwable {
        if (mIndex != null) {
            mIndex.finalize();
        }
        super.finalize();
    }

    /** Called before deleting */
    @Override
    public void destroy() throws Throwable {
        super.destroy();
        if (mIndex != null) {
            mIndex.destroy();
        }
    }
    
    /** 
     * Stores the whole algorithm into the passed file if the structure of 
     *  the dynamic cell tree was changed. This method is typically called periodically (every 1s).
     * @param serializationFile file to store the algorithm to
     * @throws java.io.IOException
     */
    public void checkModifiedAndStore(String serializationFile) throws IOException {
        serializingLock.lock();
        try {
            if (mIndex.isDynamicTreeModified()) {
                // Store algorithm to file
                this.storeToFile(serializationFile);
            }
        } finally {
            serializingLock.unlock();
        }
    }
    
    @Override
    public void beforeStoreToFile(String filepath) {
        super.beforeStoreToFile(filepath);
        mIndex.setDynamicTreeModified(false);
    }

    @Override
    public void afterStoreToFile(String filepath, boolean successful) {
        super.afterStoreToFile(filepath, successful); 
        if (!successful) {
            mIndex.setDynamicTreeModified(true);
        }
    }
    
    /**
     * Sets the period of the automatic temporary-closeable checking of the buckets.
     * @param period the new checking period in milliseconds;
     *          zero value means disable the checking
     */
    public void autoCloseBuckets(long period) {
        mIndex.autoCloseBuckets(period);        
    }
    
    // ************************      Getters and setters      ************************* //

    @Override
    public Class<? extends LocalAbstractObject> getObjectClass() {
        return mIndex.getObjectClass();
    }
    
    /** Getter for internal use within the M-Index (should be removed in future)
     * @return  */
    public MetricIndex getmIndex() {
        return mIndex;
    }

    /** 
     * Starts/stops using the pivot filtering; it can be started only if it was on during object insertion.
     * This method exists only for testing reasons.
     */
    public void setUsePivotFiltering(boolean usePivotFiltering) {
        mIndex.setUsePivotFiltering(usePivotFiltering);
    }

    /** 
     * Clears (removes) all the data stored in this M-Index. The structure of the 
     *  dynamic cell tree remains untouched.
     */
    public void clearData() throws AlgorithmMethodException {
        mIndex.clearData();
    }

    public MIndexObjectInitilizer getPPCalculator() {
        return mIndex.getPPCalculator();
    }

    @Override
    public void setOperationsThreadPool(ExecutorService operationsThreadPool) {
        super.setOperationsThreadPool(operationsThreadPool);
        mIndex.setOperationsThreadPool(operationsThreadPool);
    }        
    
    // *************************     Operation processing     *************************** //    

    /** Pre-created list of supported operations. */
    private final static List<Class<? extends AbstractOperation>> supportedOperations
            = Collections.unmodifiableList(Arrays.asList(AuxiliaryOperation.class, GetAllObjectsQueryOperation.class,
                            GetAlgorithmInfoOperation.class, GetObjectCountOperation.class));

    @Override
    public List<Class<? extends AbstractOperation>> getSupportedOperations() {
        List<Class<? extends AbstractOperation>> retVal = new ArrayList<>(supportedOperations);
        retVal.addAll(mIndex.getSupportedOpList());
        return retVal;
    }    
    
    @Override
    public <E extends AbstractOperation> List<Class<? extends E>> getSupportedOperations(Class<? extends E> subclassToSearch) {
        return Algorithm.getOperationSubClasses(getSupportedOperations(), subclassToSearch);
    }

    /**
     * This method handles all operations to be processed at this M-Index - 
     * it delegates the processing of the operation to an OperationProcessor.
     * @param operation the abstract operation to be processed
     */
    @Override
    public NavigationProcessor<? extends AbstractOperation> getNavigationProcessor(AbstractOperation operation) {
        return mIndex.getNavigationProcessor(operation);
    }


    // **************************************     Additional operations non-standard operations     *************************** //
    
    /**
     * Process any auxiliary operation on this M-Index
     * @param operation aux. operation to be processed
     * @throws messif.algorithms.AlgorithmMethodException
     */
    public void processAuxiliaryOperation(AuxiliaryOperation operation) throws AlgorithmMethodException {
        operation.process(mIndex);
    }

    /**
     * Process GetAllObjects operation
     * @param operation GAO operation
     * @throws messif.algorithms.AlgorithmMethodException
     */
    public void processGetAllObjects(GetAllObjectsQueryOperation operation) throws AlgorithmMethodException {
        for (LocalBucket bucket : mIndex.getBucketDispatcher().getAllBuckets()) {
            operation.evaluate(bucket.getAllObjects());
        }
    }

    /**
     * Process AlgorithmInfo operation
     * @param operation the operation to process
     */
    public void processAlgorithmInfo(GetAlgorithmInfoOperation operation) {
        operation.addToAnswer(toString());
        operation.endOperation();
    }

    /**
     * Process AlgorithmInfo operation
     * @param operation the operation to process
     */
    public void processObjectCount(GetObjectCountOperation operation) {
        operation.addToAnswer(mIndex.getBucketDispatcher().getObjectCount());
        operation.endOperation();
    }

    /**
     * Prints detailed information about this M-Index - including the dynamic cluster tree
     *  and numbers of objects stored in individual clusters.
     * WARNING: opens all disk buckets (files)
     * @return string representation of this M-Index
     */
    public String toRichString() {
        return mIndex.toRichString();
    }

    /**
     * Prints info about this M-Index.
     * @return string representation of this M-Index
     */
    @Override
    public String toString() {
        return mIndex.toString();
    }
}
