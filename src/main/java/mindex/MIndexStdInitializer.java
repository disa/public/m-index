/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;

/**
 * This is class is a simple extension of {@link MIndexObjectInitilizer}. It can
 * calculate PP of given objects or a collection of objects using a thread pool.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class MIndexStdInitializer extends MIndexObjectInitilizer implements ThreadPoolUser {

    /**
     * Class serial ID for serialization
     */
    private static final long serialVersionUID = 112091L;

    /**
     * Size of the PP calculation batches in each thread.
     */
    private static final int BATCH_SIZE = 500;
    
    /**
     * Thread pool service to process operations in threads.
     */
    private transient ExecutorService threadPool = null;

    /**
     * Creates the PP calculator given M-Index properties
     *
     * @param mIndexProperties properties with M-Index configuration
     * @throws InstantiationException if this part of M-Index cannot be
     * initialized using this properties
     */
    public MIndexStdInitializer(MIndexProperties mIndexProperties) throws InstantiationException {
        super(mIndexProperties);
//        if (configuration.getIntProperty(MIndexProperty.THREAD_NUMBER) > 1) {
//            threadPool = Executors.newFixedThreadPool(configuration.getIntProperty(MIndexProperty.THREAD_NUMBER));
//        }
    }

    @Override
    public ExecutorService setOperationsThreadPool(ExecutorService operationsThreadPool) {
        try {
            return threadPool;
        } finally {
            threadPool = operationsThreadPool;
        }
    }    
    
    // *************** Init object - calculate obj-pivot distance + PP ********************* //
    /**
     * Initialize the object in M-Index: calculate all distances, sort them, set
     * pivot permutation (and set filter, if the filter should be stored by this
     * M-Index).
     *
     * @param object to initialize
     * @return the initiated object
     * @throws messif.algorithms.AlgorithmMethodException
     */
    @Override
    public LocalAbstractObject initPP(LocalAbstractObject object) throws AlgorithmMethodException {
        initObject(object, isUsePivotFiltering());
        return object;
    }
    
    /**
     * Initialize the object in M-Index: calculate all distances, sort them, set
     * pivot permutation (and set filter, if the filter should be stored by this
     * M-Index).
     *
     * @param object object to initialize
     * @return distances to pivots
     */
    @Override
    public float[] initPPGetDistances(LocalAbstractObject object) throws AlgorithmMethodException {
        return initObject(object, true);
    }

    /**
     * Initializes the PP of a collection of objects. If the thread pool is created, it is done in parallel. 
     *
     * @param objects the collection of objects to initialize the PP for
     * @return collection of initialized objects - it may differ from the passed one
     * @throws messif.algorithms.AlgorithmMethodException if anything goes wrong
     */
    @Override
    public Collection<? extends LocalAbstractObject> initPP(Collection<? extends LocalAbstractObject> objects) throws AlgorithmMethodException {
        if (threadPool == null) {
            ArrayList<LocalAbstractObject> retVal = new ArrayList<>(objects.size());
            for (LocalAbstractObject localAbstractObject : objects) {
                retVal.add(initPP(localAbstractObject));
            }
            return retVal;
        }        
        
        List<InitObjectThreadCallable> initObjectsCallable = new ArrayList<>(objects.size() / BATCH_SIZE + 1);
        Iterator<? extends LocalAbstractObject> iterator = objects.iterator();
        while (iterator.hasNext()) {
            initObjectsCallable.add(new InitObjectThreadCallable(iterator));
        }
        try {
            Collection<LocalAbstractObject> retVal = new ArrayList<>(objects.size());
            for (Future<Collection<LocalAbstractObject>> future : threadPool.invokeAll(initObjectsCallable)) {                
                retVal.addAll(future.get());
            }
            return retVal;
        } catch (InterruptedException | ExecutionException e) {
            throw new AlgorithmMethodException(
                    "An init object tasks wasn't succesful!", e);
        }
    }

    /**
     * Deserialize and set the transient fields.
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
//        if (configuration.getIntProperty(MIndexProperty.THREAD_NUMBER) > 1) {
//            threadPool = Executors.newFixedThreadPool(configuration.getIntProperty(MIndexProperty.THREAD_NUMBER));
//        }
    }

    /**
     * Inner class to compute object pivot distances in a thread.
     */
    private class InitObjectThreadCallable implements Callable<Collection<LocalAbstractObject>> {

        /**
         * Object to be initialized in a thread.
         */
        private LocalAbstractObject [] objects = new LocalAbstractObject[BATCH_SIZE];

        /**
         * Constructor
         * @param iterator iterator over objects to be initialized - first {@link #BATCH_SIZE} 
         *  objects are taken from this iterator.
         */
        public InitObjectThreadCallable(Iterator<? extends LocalAbstractObject> iterator) {
            for (int i = 0; i < BATCH_SIZE; i++) {
                if (iterator.hasNext()) {
                    objects[i] = iterator.next();
                } else {
                    objects = Arrays.copyOf(objects, i);
                    break;
                }
            }
        }

        /**
         * This method runs the initPP method in a thread.
         *
         * @return initialized LocalAbstractObject
         * @throws Exception if the execution goes wrong
         */
        @Override
        public Collection<LocalAbstractObject> call() throws Exception {
            for (int i = 0; i < objects.length; i++) {
                objects[i] = initPP(objects[i]);
            }
            return Arrays.asList(objects);
        }
    }
}
