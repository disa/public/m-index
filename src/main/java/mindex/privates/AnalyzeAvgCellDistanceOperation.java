/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.privates;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import messif.algorithms.AlgorithmMethodException;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.AbstractOperation;
import mindex.MetricIndex;
import mindex.algorithms.AuxiliaryOperation;
import mindex.distance.WeightedSumDistCalculatorCorrected;
import mindex.navigation.VoronoiCell;
import mindex.navigation.VoronoiInternalCell;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
@AbstractOperation.OperationName("Research operation evaluate average query-obj distance and the error of the estimator")
public class AnalyzeAvgCellDistanceOperation extends AuxiliaryOperation {
    
    private final LocalAbstractObject query;

    private final File outputDir;

    private Map<LocalAbstractObject, Float> distanceCache;
    
    
    private static final float [] COEF_PARAMS = new float [] { 1f, 0.9f, 0.8f, 0.7f, 0.6f, 0.5f };
    
    private List<WeightedSumDistCalculatorCorrected> calculators;
    
    @AbstractOperation.OperationConstructor({"query object", "output directory"})
    public AnalyzeAvgCellDistanceOperation(LocalAbstractObject query, File outputDir) {
        this.query = query;
        this.outputDir = outputDir;
    }
    
    private float getCellDistanceEstimation(VoronoiCell cell, WeightedSumDistCalculatorCorrected calculator) {
        return calculator.getCellDistanceEstimation(cell.getPppForReading());
        //return calculator.getQueryDistance(cell.getPppForReading());
    }
    
    private float getAvgQueryCellDistance(VoronoiCell cell) {
        float sum = 0f;
        int objCount = 0;
        for (AbstractObjectIterator<LocalAbstractObject> allObjects = cell.getAllObjects(); allObjects.hasNext(); ) {
            LocalAbstractObject obj = allObjects.next();
            Float distance;
            if ((distance = distanceCache.get(obj)) == null) {
                distance = query.getDistance(obj);
                distanceCache.put(obj, distance);
            }
            sum += distance;
            objCount ++;
        }
        return sum / (float) objCount;
    }

    private void getMeanDistErrors(Map<LocalAbstractObject, Float> objectsDistances, WeightedSumDistCalculatorCorrected calculator, int level,  List<Float> meanSquareDistErrors, List<Float> meanRelativeDistErrors, MetricIndex mIndex) {
        double sumOfSDE = 0;
        double sumOfRDE = 0;
        for (Entry<LocalAbstractObject, Float> pair : objectsDistances.entrySet()) {
            Float distance = pair.getValue();
            float estimation = calculator.getCellDistanceEstimation(Arrays.copyOf(mIndex.readPPP(pair.getKey()), level));
            sumOfSDE += Math.pow(distance - estimation, 2);
            sumOfRDE += getError(distance, estimation);
        }
        meanSquareDistErrors.add((float) (sumOfSDE / (double) objectsDistances.size()));
        meanRelativeDistErrors.add((float) (sumOfRDE / (double) objectsDistances.size()));
    }
    
    private float getError(float rightDistance, float estimation) {
        if (rightDistance == 0f) {
            return Math.abs(rightDistance - estimation);
        }
        return Math.abs(rightDistance - estimation) / rightDistance;
    }
    
    protected void processOnLevel(int level, VoronoiCell node, List<List<Float>> output) {
        if (level == 0) {
            float avgQueryCellDistance = getAvgQueryCellDistance(node);
            for (int i = 0; i < calculators.size(); i++) {                
                output.get(i).add(getError(avgQueryCellDistance, getCellDistanceEstimation(node, calculators.get(i))));
            }
            return;
        }
        if (!(node instanceof VoronoiInternalCell)) {
            throw new IllegalArgumentException("processing of AnalyzeAvgCellDistanceOp assumes that the M-Index has full Vorono tree (leafs are only on the last level)");
        }
        for (Iterator<Entry<Short,VoronoiCell>> it = ((VoronoiInternalCell) node).getChildNodes(); it.hasNext();) {
            processOnLevel(level - 1, it.next().getValue(), output);
        }
    }
    
    @Override
    public void process(MetricIndex mIndex) throws AlgorithmMethodException {        
        this.calculators = new ArrayList<>(COEF_PARAMS.length);
        for (float f : COEF_PARAMS) {
            calculators.add(new WeightedSumDistCalculatorCorrected(query, mIndex, f));
        }        
        distanceCache = new HashMap<>(mIndex.getObjectCount());
        
        // do the analysis for each level
        for (int level = 1; level <= mIndex.getMaxLevel(); level++) {
            if (level <= mIndex.getMinLevel()) {
                List<List<Float>> outputErrors = new ArrayList<>(calculators.size());
                for (int i = 0; i < calculators.size(); i++) {
                    outputErrors.add(new ArrayList<Float>());            
                }
                processOnLevel(level, mIndex.getVoronoiCellTree(), outputErrors);

                // now average the errors uncovered for cells on given level (for all values of COEF)
                List<Float> avgErrors = new ArrayList<>(calculators.size());
                for (int i = 0; i < calculators.size(); i++) {
                    float avgError = 0f;
                    for (float f : outputErrors.get(i)) {
                        avgError += f;
                    }
                    avgError /= (float) outputErrors.get(i).size();
                    avgErrors.add(avgError);
                }
                printOutput(avgErrors, CELLERR_OUTPUT_FILE_PREFIX, level);
            }
            if (distanceCache.size() != mIndex.getObjectCount()) {
                throw new AlgorithmMethodException("after the first part, the dist cache should be full");
            }
            
            // calculate the MSDE (mean squared dist. error)
            List<Float> msde = new ArrayList<>(calculators.size());
            List<Float> mrde = new ArrayList<>(calculators.size());
            for (WeightedSumDistCalculatorCorrected calculator : calculators) {
                getMeanDistErrors(distanceCache, calculator, level, msde, mrde, mIndex);                
            }
            printOutput(msde, MSDE_OUTPUT_FILE_PREFIX, level);
            printOutput(mrde, MRDE_OUTPUT_FILE_PREFIX, level);
        }
    }
    
    public static final String CELLERR_OUTPUT_FILE_PREFIX = "cellDistanceErrors-l";
    public static final String MSDE_OUTPUT_FILE_PREFIX = "meanSquaredDistErr-l";
    public static final String MRDE_OUTPUT_FILE_PREFIX = "meanRelativeDistErr-l";
    
    protected void printOutput(List<Float> avgErrors, String outputPrefix, int level) throws AlgorithmMethodException {
        File outFile = new File(outputDir, outputPrefix + level + ".txt");
        try (PrintStream printStream = new PrintStream(new FileOutputStream(outFile, true))) {
            for (int i = 0; i < COEF_PARAMS.length; i++) {
                printStream.print("coef-" + COEF_PARAMS[i]);
                printStream.print(": " + avgErrors.get(i));
                if (i < COEF_PARAMS.length - 1) {
                    printStream.print(", ");
                } else {
                    printStream.println();
                }
            }
            printStream.flush();
        } catch (IOException ex) {
            throw new AlgorithmMethodException(ex.toString());
        }
    } 

    @Override
    public boolean wasSuccessful() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void endOperation() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
    
}
