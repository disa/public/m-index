/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex.privates;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.objects.LocalAbstractObject;
import messif.objects.impl.ObjectIntVectorL2;
import messif.objects.util.AbstractObjectIterator;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
public class TestIOSpeed {

    public static final String clearCacheRun = "/home/xnovak8/work/1303-pppcode/scripts/clear-cache";
    public static final int NUMBER_OF_READS = 10000;
    public static final int THREAD_POOL_SIZE = 200;
    public static final int BUFFER_SIZE = 16*1024;
    
    protected static final Random random = new Random(System.currentTimeMillis());
    
    protected static long testSequentialRead(FileChannel fileChannel, long channelSize, int nReads) throws IOException {
        long startTime = System.currentTimeMillis();
        
        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
        int sumRead = 0;
        for (int i = 0; i < nReads; i++) {
            long nextPosition = Math.abs(random.nextLong()) % channelSize;
            
            buffer.clear();
            sumRead += fileChannel.read(buffer, nextPosition);
        }
        //System.out.println("avg number of read bytes: " + (sumRead / nReads));
                
        return ((System.currentTimeMillis() - startTime));
    }

    protected static long testParallelRead(AsynchronousFileChannel asyncFileChannel, long channelSize, int nReads, int batchSize) {
        long startTime = System.currentTimeMillis();

        int readCounter = 0;
        while (readCounter < nReads) {
            Future [] futures = new Future[batchSize];
            for (int i = 0; i < batchSize && readCounter + i < nReads; i++) {
                ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);        
                long nextPosition = Math.abs(random.nextLong()) % channelSize;

                buffer.clear();
                futures[i] = asyncFileChannel.read(buffer, nextPosition);
            }
            try {
                int sumRead = 0;
                for (Future future : futures) {
                    sumRead += (Integer) future.get();
                }
                //System.out.println("avg number of read bytes: " + (sumRead / futures.length));
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(TestIOSpeed.class.getName()).log(Level.SEVERE, null, ex);
            }
            readCounter += batchSize;
        }
        
        return (System.currentTimeMillis() - startTime);        
    }

    
    protected static BlockingQueue<ResultCreator> bufferPool = new ArrayBlockingQueue<>(THREAD_POOL_SIZE);
    
    /**
     * Class that manages a buffer and creates an object from data read to the buffer. The result is stored to the queue
     *  of results.
     */
    protected static class ResultCreator implements CompletionHandler<Integer, BlockingQueue<LocalAbstractObject>>{
        private ByteBuffer buffer;
        
        public ResultCreator(ByteBuffer buffer) {
            this.buffer = buffer;
        }                
        
        public void clearBuffer() {
            buffer.clear();
        }

        public ByteBuffer getBuffer() {
            return buffer;
        }
        
        @Override
        public void completed(Integer result, BlockingQueue<LocalAbstractObject> results) {
            try {
                // TODO: create LocalAbstractObjects from the ByteBuffer
                results.put(new ObjectIntVectorL2(new int [] {1,2,3,4}));
                bufferPool.put(this);
            } catch (InterruptedException ex) {
                Logger.getLogger(TestIOSpeed.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void failed(Throwable exc, BlockingQueue<LocalAbstractObject> results) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
    
    /**
     * Blocking iterator to return the objects read from the disk storage.
     */
    protected static class BlockingIterator extends AbstractObjectIterator<LocalAbstractObject> {
        /**
         * Shared queue buffer of created objects.
         */
        private BlockingQueue<LocalAbstractObject> results = new LinkedBlockingQueue<>();        

        /**
         * Counter of objects to be read from the queue (expected size)
         */
        protected int expectedObjCount;
        
        /**
         * Flag for remembering if next() has been called.
         */
        protected int hasNext = -1;

        private LocalAbstractObject currentObject;

        public BlockingQueue<LocalAbstractObject> getResults() {
            return results;
        }                
        
        public BlockingIterator(int expectedObjCount) {
            this.expectedObjCount = expectedObjCount;
        }

        @Override
        public LocalAbstractObject getCurrentObject() throws NoSuchElementException {
            if (currentObject == null) {
                throw new NoSuchElementException("method next() was not called yet");
            }
            return currentObject;
        }

        @Override
        public boolean hasNext() {
            if (expectedObjCount <= 0) {
                hasNext = 0;
                return false;
            }

            if (hasNext == -1) {
                try {
                    // this is a blocking operation
                    currentObject = results.take();
                    hasNext = 1;
                } catch (InterruptedException ex) {
                    Logger.getLogger(TestIOSpeed.class.getName()).warning("thread interrupted while waiting for 'readObjects': " + ex);
                    hasNext = 0;
                }
            }
            return hasNext == 1;
        }

        @Override
        public LocalAbstractObject next() {
            if (hasNext != 1) {
                throw new NoSuchElementException("There are no more objects");
            }
            hasNext = -1;
            expectedObjCount--;
            return currentObject;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported.");
        }
    }

    protected static long testContinuousParallelRead(final AsynchronousFileChannel asyncFileChannel, final long channelSize, final int nReads) {
        // initialization done once per storage
        for (int i = 0; i < THREAD_POOL_SIZE; i++) {
            bufferPool.add(new ResultCreator(ByteBuffer.allocate(BUFFER_SIZE)));
        }
        
        final BlockingIterator retVal = new BlockingIterator(nReads);
        
        // run thread that initiates the asynchronous read
        new Thread() {
            @Override
            public void run() {
                for (int readCounter = 0; readCounter < nReads; readCounter++) {
                    long nextPosition = Math.abs(random.nextLong()) % channelSize;
                    try {
                        ResultCreator creator = bufferPool.take();
                        creator.clearBuffer();
                        asyncFileChannel.read(creator.getBuffer(), nextPosition, retVal.getResults(), creator);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(TestIOSpeed.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }.start();
        
        System.out.println("\t(starting reading results from iterator)");
        long startTime = System.currentTimeMillis();
        while (retVal.hasNext()) {
            LocalAbstractObject next = retVal.next();
            next.getDistance(next);
        }
        //System.out.println("avg number of read bytes: " + (readBytesCounter.get() / nReads));
        return (System.currentTimeMillis() - startTime);        
    }
    
    protected static void clearCaches() throws IOException {
        Process process = new ProcessBuilder(clearCacheRun).start();
        
        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        System.out.println("Cleaning caches: ");
        String line;        
        while ((line = br.readLine()) != null) {
            System.out.println("\t" + line);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Usage: TestIOSpeed <file_to_read>");
            return;
        }

        File file = new File(args[0]);
        if (! file.exists()) {
            System.err.println("file not exists or cannot be read: " + file);
            return;
        }
        try {
            long size;
            try (FileChannel fileChannel = new RandomAccessFile(file, "r").getChannel()) {
                size = fileChannel.size();
                clearCaches();
                System.out.println("file size: " + size);
                System.out.println("Time of sequential reads of " + NUMBER_OF_READS +" blocks: " 
                        + testSequentialRead(fileChannel, size, NUMBER_OF_READS));
            }
            
            int [] batchSizes = new int [] {10, 20, 50, 100, 200, 500, 1000};

            try (AsynchronousFileChannel asyncFileChannel = AsynchronousFileChannel.open(file.toPath(), StandardOpenOption.READ)) {
                for (int batchSize : batchSizes) {
                    clearCaches();
                    System.out.println("Time of async reads of " + NUMBER_OF_READS + " blocks (batch size = " + batchSize + ") : "
                            + testParallelRead(asyncFileChannel, size, NUMBER_OF_READS, batchSize));
                }
            }
            
            try (AsynchronousFileChannel asyncFileChannel = AsynchronousFileChannel.open(file.toPath(), Collections.singleton(StandardOpenOption.READ), Executors.newFixedThreadPool(THREAD_POOL_SIZE))) {
                clearCaches();
                System.out.println("Time of continuous async reads of " + NUMBER_OF_READS + " blocks: "
                        + testContinuousParallelRead(asyncFileChannel, size, NUMBER_OF_READS));
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestIOSpeed.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestIOSpeed.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
