/*
 *  This file is part of M-Index library: http://disa.fi.muni.cz/m-index/
 *
 *  M-Index library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  M-Index library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with M-Index library.  If not, see <http://www.gnu.org/licenses/>.
 */
package mindex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import mindex.ParameterImpl.ParamClass;

/**
 * Enumeration of all M-Index parameters used in M-Index properties {@link MIndexProperties}
 *  It stores <ul>
 *  <li>property key</li>
 *  <li>value type</li>
 *  <li>default value</li>
 *  <li>comment text</li>
 *  <li>flag saying if the a this param should be used via a variable to be passed from the messif-start script</li>
 * </ul>
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, novak.david@gmail.com
 */
 public enum MIndexProperty implements Parameter {
    
     DATA_CLASS(ParamClass.MANDATORY, "object_class", Type.STRING, "", "# the class of objects for which is this M-Index created", true),
     
     PIVOT_FILE(ParamClass.MANDATORY, "pivot.file", Type.STRING, "", "# the file with the pivots", true),    
     
     NUMBER_OF_PIVOTS ("pivot.number", Type.SHORT, "-1", "# if specified (and positive), then this number is taken regardless of content of the pivot file"),    
     
     MIN_LEVEL ("minlevel", Type.SHORT, "1", "# the minimum dynamic M-Index level"),
     
     MAX_LEVEL ("maxlevel", Type.SHORT, "8", "# the maximum dynamic M-Index level"),
     
//     PPCALCULATOR_TYPE ("ppcalculator.class", Type.STRING, "mindex.SequentialPPCalculator", "# class of PP calculator to be created during the M-Index initialization"),
     
//     THREAD_NUMBER (ParamClass.OTHER, "ppcalculator.threads", Type.INT, "1", "# of threads to be used for object-pivot calculation; if lower then two, no thread-pool is created", true),

     USE_EXISTING_PP (ParamClass.MANDATORY, "use_existing_pivot_permutation", Type.BOOL, "true", "# use precomputed pivot permutation, if passed from the outside"
            + "\n#   set to true if using MapReduce PP calculator"),
     
     PIVOT_FILTERING (ParamClass.OTHER, "use_pivot_filtering", Type.BOOL, "false", "# store the precomputed distances for pivot filtering (increases I/O, decreases comp. costs)", true),

     CHECK_DUPLICATE_DC (ParamClass.OTHER, "check_duplicate_dc", Type.BOOL, "false", "# if set to true then the query processor ensures that each data object\n"
             + "#  is processed only once (this makes sense only if multi-overlay M-Index used)", true),
     
//     MAX_OBJECT_NUMBER ("max_object_number", Type.LONG, "0", "# max number of objects to be stored in this index; not positive number means unbounded"),
     
     BUCKET_MAX_CAPACITY (ParamClass.MANDATORY, "bucket.capacity", Type.LONG, "1024", "# capacity of the buckets in lowest M-Index level \n"
            + "# when exceeded, M-Index dynamic tree is split to the next level (set as bucket soft capacity)"),
     
     BUCKET_MIN_OCCUPATION ("bucket.min_occupation", Type.LONG, "0", "# minimal occupation of a self-standing bucket (if smaller, a \"multi-bucket\" is created)"),
     
     OCCUPATION_IN_BYTES ("bucket.occupation_as_bytes", Type.BOOL, "false", "# occupation is counted either as number of objects or bytes (not precise)"),
     
     AGILE_SPLIT ("bucket.agile_split", Type.BOOL, "false", "# it desides between the following behaviour of the minimimu occupation:\n"
            + "#  true = every multi-bucket that can be split is split (if the new ones will have over minimum occupation)\n"
            + "#  false = the multi-bucket is split only when acceeds the max limit AND only the largest partition is put separately"),
     
     BUCKET_CLASS (ParamClass.MANDATORY, "bucket.class", Type.STRING, "messif.buckets.impl.MemoryStorageBucket", "# class of buckets created at the cell tree leaf nodes"),

     PRECISE_SEARCH ("precise.search", Type.BOOL, "false", "# if true, the index is better configured for precise search (e.g. the cells keep track of key interval)"),
     
     APPROX_FORCE_DEFAULT ("approximate.force_default", Type.BOOL, "false", "# if yes, the operation is settings is ignored"),

     APPROX_DEFAULT ("approximate.precision", Type.INT, "10000", "# default parameter of the approximate operation"),
     
     APPROX_DEFAULT_TYPE ("approximate.type", Type.STRING, "ABS_OBJ_COUNT", "# default type of approximate precision parameter"),
          
     APPROX_PROCESS_WHOLE_MULTIBUCKET ("approximate.process_whole_multi_bucket", Type.BOOL, "true", "# if the multi-buckets are used (see min_occupation) then this flag differentiates between two modes\n"
            + "#  1) either the approximate search reads and processes always the whole multi-bucket (true)\n"
            + "#  2) the cache is used and the multi-bucket is partitioned according to object keys and only relevant data is processed (false)"),
     
     APPROX_CHECK_KEYINTERVAL ("approximate.check_key_interval", Type.BOOL, "false", "# if true, then the M-Index keys are read from the data in bucket and check against query interval before processing"),
          
     REMOVE_DUPLICATES ("remove_duplicates", Type.BOOL, "false", "# if true, then inserted data are checked for duplicates and every object is then maximally once in the stored data"),
     
//     PPP_CODES ("ppp_codes", Type.BOOL, "false", "# is this the \"PPPCodes\" variant of the M-Index \n"
//            + "#  if this flag is true, then the following flags are automatically considered as \"false\"\n"
//            + "#   1) use_pivot_filtering, 2) precise.search, 3) overfilled.bucket.special, \n"
//            + "#   4) approximate.check_key_interval,  5) approximate.process_whole_multi_bucket"),
     
     BUCKET_OVERFILLED_SPECIAL ("overfilled.bucket.special", Type.BOOL, "false", "# if set true, overfilled buckets, that cannot be split, use a special class (typically another M-Index)"),

     DISK_BUCKET_PARAM_ONESTORAGE (ParamClass.DISK_STORAGE, "oneStorage", Type.BOOL, "false", ""),
     DISK_BUCKET_DIR (ParamClass.DISK_STORAGE, "dir", Type.STRING, "diskbuckets", "", true),
     DISK_BUCKET_CACHE (ParamClass.DISK_STORAGE, "cacheClasses", Type.STRING, "<diskbucketcache:messif.objects.keys.AbstractObjectKey,mindex.MIndexPPSimple,mindex.MIndexKeySimple,mindex.MIndexKeyPPFull,messif.objects.PrecomputedDistancesFixedArrayFilter>", 
             "# the classes that will be often serialized and thus their names should be cached", false),
     
     OVERFILLED_BUCKET_CLASS (ParamClass.OVERFILLED, "overfilled.bucket.class", Type.STRING, "messif.buckets.impl.AlgorithmStorageBucket", ""),
     OVERFILLED_BUCKET_PARAMS_CLASS (ParamClass.INTERNALMINDEX, "class", Type.STRING, "mindex.algorithms.MIndexAlgorithm", ""),
     // 1st parameter of the MIndexAlgorithm constructor
     OVERFILLED_BUCKET_PARAMS_PARAM_1 (ParamClass.INTERNALMINDEX, "param.1", Type.STRING, "THESE_PROPERTIES", ""),
     // 2nd parameter of the MIndexAlgorithm constructor
     OVERFILLED_BUCKET_PARAMS_PARAM_2 (ParamClass.INTERNALMINDEX, "param.2", Type.STRING, "inner.", "")
     ;
    
     
     // ***********************               Fields            ********************** //
     
     private ParameterImpl parameter;
     
     @Override
     public String getKey() {
         return parameter.getKey();
     }

     @Override
     public Type getType() {
         return parameter.getType();
     }

     @Override
     public String getDefaultValue() {
         return parameter.getDefaultValue();
     }

     @Override
     public String getComment() {
         return parameter.getComment();
     }

     public boolean useMessifStartVariable() {
         return parameter.useMessifStartVariable();
     }

     public String getMessifStartVariable() {
         if (! useMessifStartVariable()) {
             return null;
         }
         return name().toLowerCase().replaceAll("_", "");
     }
     
     public ParamClass getParamClass() {
         return parameter.getParamClass();
     }


     
     /**
      * Internal private constructor setting all fields.
      * 
      * @param key property key of this parameter
      * @param type type of the parameter
      * @param defaultValue default string
      * @param comment comment string (can be empty)
      * @param useMessifStartVariable if true, value of the parameter is not set directly in the .cf file but is passed from the .defaults file as a variable
      */
     private MIndexProperty(ParamClass paramClass, String key, Type type, String defaultValue, String comment, boolean useMessifStartVariable) {
         parameter = new ParameterImpl(paramClass, key, type, defaultValue, comment, useMessifStartVariable);
     }

     private MIndexProperty(String propertyString, Type type, String defaultValue, String comment) {
         this(ParamClass.OTHER, propertyString, type, defaultValue, comment, false);
     }

     private MIndexProperty(ParamClass paramClass, String propertyString, Type type, String defaultValue, String comment) {
         this(paramClass, propertyString, type, defaultValue, comment, false);
     }
     
     
     /**
      * Returns the list of M-Index parameters corresponding to at least one of given param classes {@link ParamClass}.
      * @param paramClass one or more parameter classes
      * @return list of corresponding parameters
      */
     public static Collection<MIndexProperty> getParams(ParamClass ... paramClass) {
         Collection<MIndexProperty> retVal = new ArrayList<>();
         for (MIndexProperty mIndexProperty : MIndexProperty.values()) {
             for (ParamClass allowedClass : paramClass) {
                if (mIndexProperty.getParamClass().equals(allowedClass)) {
                    retVal.add(mIndexProperty);
                    break;
                }
             }
         }
         return retVal;
     }

     public static final String PREFIX_BUCKET_PARAMS = "bucket.params.";
     public static final String PREFIX_OVERFILLED_BUCKET_PARAMS = "overfilled.bucket.params.";

     public static final String MEMORY_BUCKET = "messif.buckets.impl.MemoryStorageBucket";
     public static final String DISK_BUCKET = "messif.buckets.impl.DiskBlockBucket";
     public static final String INTERNAL_MINDEX_BUCKET = "messif.buckets.impl.AlgorithmStorageBucket";
     
     /**
      * Returns the list of M-Index parameters that correspond to given disk bucket class (as string).
      * @param bucketClassString class of bucket to be created within M-Index
      * @return list of M-Index parameters 
      */
     public static Collection<MIndexProperty> getBucketParams(String bucketClassString) {
         switch (bucketClassString) {
             case MEMORY_BUCKET:
                 return Collections.EMPTY_LIST;
             case DISK_BUCKET:
                 return getParams(ParamClass.DISK_STORAGE);
             case INTERNAL_MINDEX_BUCKET:
                 return getParams(ParamClass.INTERNALMINDEX);
             default:
                 return Collections.EMPTY_LIST;
         }
     }
     
}
